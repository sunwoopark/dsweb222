<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>대전광역시 서구 시각화 서비스 관리자</title>
<link rel="stylesheet" href="/common/css2/style.css">


<style>
div.updateModal {
	position: relative;
	z-index: 1;
	display: none;
}

div.modalBackground {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: rgba(0, 0, 0, 0.8);
	z-index: -1;
}

div.modalContent {
	position: fixed;
	top: 20%;
	left: calc(50% - 250px);
	width: 500px;
	height: 150px;
	padding: 20px 10px;
	background: #fff;
	border: 2px solid #666;
}

div.modalContent button {
	font-size: 20px;
	padding: 5px 10px;
	margin: 10px 0;
	background: #fff;
	border: 1px solid #ccc;
}
</style>


<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
	<div id="wrap">
		<header id="header">
			<div class="static">
				<h1 class="logo">
					<a href="#"><img src="/images/h-logo.png" alt="대전광역시 서구">
						<span>시각화 서비스 관리자</span></a>
				</h1>
				<span class="logo2">행복동행대전서구</span>
			</div>
		</header>
		<div id="container" class="sub">
			<div class="contents">
				<h3 class="title agC">서구청 시각화 서비스 관리자</h3>
				<table class="table">
					<caption>서구청 시각화 서비스 관리자</caption>
					<colgroup>
						<col style="width: 18%">
						<col style="width: 13%">
						<col style="width: 14%">
						<col style="width: auto">
						<col style="width: 8%">
						<col style="width: 8%">
						<col style="width: 8%">
					</colgroup>
					<thead>
						<tr>
							<th>화면좌측 대제목</th>
							<th>본문 상단 메뉴</th>
							<th>본문 지도 컨텐츠 세부메뉴</th>
							<th>본문 컨텐츠 제목</th>
							<th>Download</th>
							<th>Update</th>
							<th>Guide</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th rowspan="4">교통/주차 관련 민원 분석 현황</th>
							<td rowspan="2">교통민원분석</td>
							<td rowspan="2"></td>
							<td class="agL">- 교통 민원 키워드 분석 및 상위10개 키워드</td>
							<td><a href="#" onclick="fn_fileDown('a_1');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('a_1')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 연도별 및 신청경로별 민원 현황</td>
							<td><a href="#" onclick="fn_fileDown('a_2');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('a_2')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="2">주차민원분석</td>
							<td rowspan="2"></td>
							<td class="agL">- 주차 민원 키워드 분석 및 상위10개 키워드</td>
							<td><a href="#" onclick="fn_fileDown('b_1');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('b_1')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 연도별 및 신청경로별 민원 현황</td>
							<td><a href="#" onclick="fn_fileDown('b_2');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('b_2')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<th rowspan="4">교통사고 원인 및 현황</th>
							<td rowspan="4">교통사고현황</td>
							<td rowspan="4"></td>
							<td class="agL">- 사고내용별 교통사고 발생 건수</td>
							<td><a href="#" onclick="fn_fileDown('c_1');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('c_1')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 사고내용별 어린이 교통사고 발생 건수</td>
							<td><a href="#" onclick="fn_fileDown('c_2');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('c_2')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 월별 교통사고 발생 건수</td>
							<td><a href="#" onclick="fn_fileDown('c_3');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('c_3')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 요일별 교통사고 발생 건수</td>
							<td><a href="#" onclick="fn_fileDown('c_4');">Download</a><br>
							</td>
							<td><a href="#" target="_blank" onclick="openPop('c_4')">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>

						<tr>
							<td colspan="7">정리중</td>
						</tr>

						<tr>
							<th rowspan="3">소화전 주변 불법 주차 단속 현황</th>
							<td rowspan="3">불법주차단속</td>
							<td rowspan="3"></td>
							<td class="agL">- 소화전 주변 불법 주차 단속 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 소화전 개수</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 연도별/월별 불법주차 단속 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<th rowspan="2">교통과태료 현황</th>
							<td rowspan="2">과태료 현황</td>
							<td rowspan="2"></td>
							<td class="agL">- 행정동별 대상 건물 수</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 평균 부과금액 및 대상건물수 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<th rowspan="7">장애인 주차구역 불법주정차</th>
							<td rowspan="7">장애인 주차구역</td>
							<td rowspan="2">교통CCTV설치(지도)</td>
							<td class="agL">- 장애인 주차 구역 불법 주정차 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 불법주정차 CCTV 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="3">불법주정차과태료(지도)</td>
							<td class="agL">- 장애인 주차 구역 불법 주정차 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 월별 장애인 주차구역 불법주차 단속 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 단속 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="2">주차장위치(지도)</td>
							<td class="agL">- 장애인 주차 구역 불법 주정차 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 장애인 주차장 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<th rowspan="8">교통 보행환경 개선 및 CCTV현황</th>
							<td rowspan="4">교통보행환경 개선</td>
							<td rowspan="2">어린이/노인(지도)</td>
							<td class="agL">- 교통보행환경(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 어린이/노인 시설 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="2">육교주변(지도)</td>
							<td class="agL">- 교통보행환경(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 육교 설치 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="4">CCTV 설치우선 지역</td>
							<td rowspan="2">교통CCTV(지도)</td>
							<td class="agL">- 교통보행환경(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 교통CCTV 설치 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="2">방범CCTV(지도)</td>
							<td class="agL">- 교통보행환경(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 방범 CCTV 설치 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<th rowspan="15">화물차량 및 무단방치차량 현황</th>
							<td rowspan="4">화물차 밤샘주차</td>
							<td rowspan="4"></td>
							<td class="agL">- 화물차 밤샘주차 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 분기별 위반 현황 Download</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 월별 화물차 밤샘주차단속현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 밤샘주차 단속 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="8">화물차운송사업자</td>
							<td rowspan="4">개인화물(지도)</td>
							<td class="agL">- 화물차 운송사업자 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 사업자 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 화물차운송사업자 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 화물차 운송유형별 비중</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="4">일반화물(지도)</td>
							<td class="agL">- 화물차 운송사업자 현황(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 사업자 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 화물차운송사업자 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 화물차 운송유형별 비중</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td rowspan="3">무단방치차량 단속</td>
							<td rowspan="3"></td>
							<td class="agL">- 무단방치차량 단속(지도)</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 연도별 월별 무단방치차량 단속 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
						<tr>
							<td class="agL">- 행정동별 무단방치차량 현황</td>
							<td><a href="#">Download</a></td>
							<td><a href="#">Update</a></td>
							<td><a href="#">View</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<footer id="footer"> </footer>
	</div>

	<!-- <div class="updateModal">
		<div class="modalContent">
			<div>
				<input type="file" name="files" id="files"> <br>
				<button type="button" class="update_btn_update">전송</button>
				<button type="button" class="update_btn_cancel">취소</button>
			</div>
		</div>
		<div class="modalBackground"></div>
	</div>
	<script>
		$(document).on("click", ".update_btn", function() {
			$(".updateModal").attr("style", "display:block;");
			var test = a11;
		});
		$(".update_btn_cancel").click(function() {
			$(".updateModal").attr("style", "display:none;");
		});
	</script>
	<script>
		$('.update_btn_update').on('click', function() {
			var formData = new FormData(); // HTML5
			var inputFile = $("input[name='files']");
			var files = inputFile[0].files;

			for (var i = 0; i < files.length; i++) {
				formData.append("file", files[i])
			}

			$.ajax({
				type : "POST",
				enctype : 'multipart/form-data',
				url : "/fileUploadAction.do",
				processData : false,
				contentType : false,
				data : formData,
				success : function(data) {
					location.href = "/table.do"
					alert('파일등록 성공');
				},
				error : function(data) {
					alert('파일등록 실패');
				}
			});
		});
	</script> -->
	<script>
		function openPop(arg1) {
			var popup = window.open("fileInsert.do?arg1=" + arg1, '파일등록',
					'width=400px,height=400px,scrollbars=yes');

		}
	</script>
	<script>
		function fn_fileDown(fileCate) {
			location.href = "fileDown.do?FILE_cate=" + fileCate;
		}
	</script>


</body>
</html>