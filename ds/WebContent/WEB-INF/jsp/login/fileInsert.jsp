<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>대전광역시 서구 시각화 서비스 관리자</title>
<link rel="stylesheet" href="/common/css2/style.css">
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


</head>
<body>

	<input type="file" name="files" id="files">
	<br>
	<button type="button" class="update_btn_update">전송</button>
	<button type="button" class="update_btn_cancel">취소</button>
	<input type="hidden" value="${arg1 }" name="arg1">

	<script>
		$('.update_btn_update').on('click', function() {
			var formData = new FormData(); // HTML5
			var inputFile = $("input[name='files']");
			var files = inputFile[0].files;

			for (var i = 0; i < files.length; i++) {
				formData.append("file", files[i])
			} 
			formData.append("arg1", "${arg1}");
			
			$.ajax({
				type : "POST",
				enctype : 'multipart/form-data',
				url : "/fileUploadAction.do",
				processData : false,
				contentType : false,
				data : formData,
				success : function(data) {
					alert('파일등록 성공');
					window.open('','_self').close();
				},
				error : function(data) {
					alert('파일등록 실패');
				}
			});
		});
	</script>

	<c:if test="${test == 1 }">
		<hr>
		<a href="csvFileInsert.do?FILE_cate=" + ${arg1}>insertFile</a>
	</c:if>



</body>
</html>