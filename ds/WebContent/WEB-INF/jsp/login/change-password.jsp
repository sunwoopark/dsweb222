<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	    <title>대전시 관리자</title>

		<!-- UI Definition -->
		<!--  CSS -->
		<link rel="stylesheet" href="/common/css/jquery-ui.css">
	    <link rel="stylesheet" href="/common/css/style.css">

		<!-- jQuery -->
		<script src="/common/js/jquery-3.2.1.min.js"></script>
	    <script src="/common/js/jquery-ui.js"></script>
	    <script src="/common/js/common.js"></script>

    	<!-- Biz Process -->
	    <script src="/js/custom.js"></script>
	    
	    <%	
	        /* Session KEY */
			String sSessionKeyUserId = "" ;
						
			/* Session 정보 가져오기 */
			sSessionKeyUserId = (String) session.getAttribute("sessionKeyUserId");
	
			if( sSessionKeyUserId == null ) sSessionKeyUserId = "";
		
			String url = request.getAttribute("javax.servlet.forward.request_uri").toString();
        %>
	    
	</head>
	<body>
		<p id="accessibility"><a href="#container">본문바로가기</a></p>
		<div id="wrap">
			<header id="header">
		        <div class="static">
		            <h1><a href="#">대전시</a></h1>
		            <div>
		               <h2>비밀번호 변경</h2>
		            </div>
		        </div>
		    </header>
			<div id="container">
				<form id="frm">
					<div class="contents login-content">
			            <div class="login-info-find pass-change">
			            	<div class="tit-msg">
			            		<p>비밀번호를 변경하는 페이지 입니다</p>
			            	</div>
			            	<label for="u-email">변경 비밀번호</label>
			            		<input type="password" id="user_passwd" name = "user_passwd" class="ipt" placeholder="* 영문, 숫자 포함 8자 이상">
				            	<label for="u-name">비밀번호 확인</label>
				            	<div class="pass-re">
				            		<input type="password" id="user_passwd2" class="ipt">
				            	</div>
				            	<button type="button" class="btn-lg blue" onclick="fn_chngPassword() ; return false ;">변경하기</button>
						</div>
					</div>
					<!-- //contents -->
				</form>  
			</div>
			<!-- //container -->
			
	    	<footer id="footer">
<address>(우)35238 대전광역시 서구 둔산서로 100(둔산동)</address><span class="tel">대표전화 : 042)288-2114/ 팩스 : 042)288-5900</span>
            <span class="copy">Copyright (C) 2014 Seo-gu Office. All rights reserved.</span>	    	</footer>
		</div>
		<!-- wrap -->
		
		<!-- 로그인 안되어 있을 경우. 사용자 action 후 이동 -->
		<div class="layer-pop" id="needLogin">
			<div class="alert-pop">
				<div class="alert-pop-head">
					<button type="button" class="alert-close">닫기</button>
				</div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong class="tx2">로그인이 필요한 화면입니다.</strong>
					</div>
					<div class="btm">
						<div class="btm-btns">
							<button type="button" class="btn-md black" id="needLoginfirm">확인</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- 비밀번호 변경 후. 사용자 action 후 이동 -->
		<div class="layer-pop" id="changeComplete">
			<div class="alert-pop">
				<div class="alert-pop-head">
					<button type="button" class="alert-close">닫기</button>
				</div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong>비밀번호 변경에 성공하였습니다.</strong>
						<p>로그인 화면으로 이동합니다.</p>
					</div>
					<div class="btm">
						<div class="btm-btns">
							<button type="button" class="btn-md black" id="completeConfirm" >확인</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<%@ include file="/WEB-INF/include/include-layerpopup.jspf" %>
		
		<script type="text/javascript">
			"use strict" ;
			
			$(document).ready(function(){
				if (!"<%= sSessionKeyUserId %>") {
	    			// 비로그인 시 처리
	    			fn_openLayerPop("needLogin") ;
	    			
	    		}
				// 로그인이 필요한 화면입니다 완료 확인 button click
				$("#needLoginfirm").on("click",function() {
					$('#needLogin').hide();
					location.href = "/login.do" ;
				});
				
				// 로그인이 필요한 화면입니다 완료 확인 button click
				$("#completeConfirm").on("click",function() {
					$('#changeComplete').hide();
					location.href = "/login.do" ;
				});
				
				
			});
			
			/* Password 변경 처리 */
			function fn_chngPassword() {
				
				// 비밀번호 정합성 Check
				// 비밀번호 정합성 Check용 변수
				const pattern1 = /[0-9]/;	// 숫자
				const pattern2 = /[a-z]/;	// 소문자
				
				if(!$("#user_passwd").val()) {
					// 앞 비밀번호 입력 Check
					fn_openType2LayerPop("user_passwd") ;
					return ;
				} else if(!$("#user_passwd2").val()) {
					// 뒤 비밀번호 입력 Check
					fn_openType2LayerPop("user_passwd") ;
					return ;
				} else if ($("#user_passwd").val().length < 8 ) {
					// 길이 Check
					fn_openType2LayerPop("passwdLengthChk") ;
					return ;
				} else if ($("#user_passwd").val() != $("#user_passwd2").val()) {
					// 비밀번호 동일성 Check
					fn_openType2LayerPop("diffPasswod") ;
					return ;
				} else if (!pattern1.test($("#user_passwd").val()) || !pattern2.test($("#user_passwd").val())){
					// 소문자와 숫자 포함
					fn_openType2LayerPop("passwdCombChk") ;
					return ;
				}
				
				// 비밀번호 변경
				const comAjax = new ComAjax("frm");
					
				comAjax.setUrl("<c:url value='/proc-change-password.do' />");
				comAjax.setCallback("fn_chngPasswordCallback");
					
				comAjax.ajax();
				
			}
			
			function fn_chngPasswordCallback(data) {
				
				const vResultCode = data.resultCode ;
				const vResultMessage = data.resultMsg ;
				const vPagePageAthr = data.pageAthr ;
					
				if (vResultCode != "ok") {
					fn_openType2LayerPop("changeFail") ;
					return ;
				}
					
				fn_openLayerPop("changeComplete") ;
				
			}
			
			/* 로그인이 필요한 화면입니다. Layer Popup Open
			   사용자 Action이 필요하여 별도로 처리 */
			function fn_openLayerPop(pType) {
				switch (pType) {
				case "needLogin":
					$('#needLogin').show();
					break ;
				case "changeComplete":
					$('#changeComplete').show();
					break ;
				default:
					break ;
				}
			}
			
			// Type2 Layer Popup 공통 처리
			// Message가 두 줄인 경우
			function fn_openType2LayerPop(pType) {
				let msg1 ;
				let msg2 ;
				
				switch (pType) {
				case "user_passwd":
					msg1 = "앞 변경 비밀번호를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "user_passwd2":
					msg1 = "뒤 변경 비밀번호를 입력하세요." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "passwdLengthChk":
					msg1 = "비밀번호의 길이는 8자리 이상이어야 합니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "diffPasswod":
					msg1 = "비밀번호가 다릅니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "changeFail":
					msg1 = "비밀번호변경에 실패하였습니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				case "passwdCombChk":
					msg1 = "비밀번호는 소문자와 숫자를 포함해야 합니다." ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break;
				default:
					break ;
				}
				
				$("#type2Msg1").html(msg1) ;
				$("#type2Msg2").html(msg2) ;
				
				$("#layerPopType2").show() ;
			}
			
		</script>
		
	</body>
</html>