<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>대전시 관리자</title>
		
		<!-- UI Definition -->
		<!--  CSS -->
		<link rel="stylesheet" href="/common/css/jquery-ui.css">
	    <link rel="stylesheet" href="/common/css/style.css">
		
		<!-- jQuery -->
		<script src="/common/js/jquery-3.2.1.min.js"></script>
	    <script src="/common/js/jquery-ui.js"></script>
	    <script src="/common/js/common.js"></script>
		
		<!-- Biz Process -->
		<script src="/js/custom.js"></script>
		
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	
	</head>
	<body>
		<p id="accessibility"><a href="#container">본문바로가기</a></p>
		
		<div id="wrap">
			<header id="header">
				<div class="static">
					<h1><a href="#">대전시</a></h1>
					<div>
						<h2>ID 찾기</h2>
					</div>
	        	</div>
	        </header>
	        <form id="frm">
		        <div id="container">
		        	<div class="contents login-content">
		        		<div class="login-info-find mail-find">
		        			<label for="u-name">이름</label>
		        			<input type="text" id="user_nm" name="user_nm" class="ipt">
		        			<label for="tel">연락처</label>
		        			<div class="tel-wp">
		        				<select id = "user_tono" name="user_tono">
									<option value="">지역번호</option>
									<c:forEach var="tonolList" items="${tonolList}" varStatus="status">
										<option value="${tonolList.minor_cd }">${tonolList.minor_nm }</option>
									</c:forEach>
								</select>
		        				<input type="text" id="user_tlno1" name="user_tlno1">
		        				<input type="text" id="user_tlno2" name="user_tlno2">
		        			</div>
		        			<button type="button" class="btn-lg blue" onclick="fn_findEmail() ; return false ;">ID 찾기</button>
		        		</div>
		        	</div>
		        </div>
	        </form>
		</div>	<!-- #wrap -->
		
		<!-- Layer Popup -->
		<!-- 가입 이메일 List -->
		<div class="layer-pop" id="findEmail" >
			<div class="alert-pop">
				<div class="alert-pop-head">
					<button type="button" class="alert-close">닫기</button>
				</div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong>해당정보로 가입된 ID 리스트 입니다.</strong>
					</div>
				<div class="btm">
					<p class="tx-blue" id="emailInfo">
						example@korea.kr<br>
                		asdase@korea.kr
					</p>
            		<div class="btm-btns">
            			<button type="button" class="btn-md black" onclick="fn_closeLayerPop('findEmail'); return false ;">확인</button>
            		</div>
            	</div>
            </div>
           </div>
		</div>
		
		<!-- 가입 이메일이 없을 경우 -->
		<div class="layer-pop" id="noEmail">
			<div class="alert-pop">
				<div class="alert-pop-head"><button type="button" class="alert-close">닫기</button></div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong>해당 정보로 가입된 계정은 없습니다.</strong>
						<p>다시 확인하여 주시기 바랍니다.</p>
					</div>
				<div class="btm">
            		<div class="btm-btns">
            			<button type="button" class="btn-md black" onclick="fn_closeLayerPop('noEmail'); return false ;">확인</button>
            		</div>
            	</div>
            </div>
           </div>
		</div>

	</body>
	
	<script type="text/javascript">
		"use strict" ;
		
		// 사용자 정보로 Email Address 가져오기
		function fn_findEmail() {
			
			if (!$("#user_nm").val()) {
				alert("이름을 입력하세요!") ;
				$("#user_nm").focus() ;
			}
			
			if (!$("#user_tono").val()) {
				alert("지역번호를 선택하세요!") ;
				$("#user_tono").focus() ;
			}
			
			if (!$("#user_tlno1").val()) {
				alert("연락처를 입력하세요!") ;
				$("#user_tlno1").focus() ;
			}
			
			if (!$("#user_tlno2").val()) {
				alert("연락처를 입력하세요!") ;
				$("#user_tlno2").focus() ;
			}
			
			let comAjax = new ComAjax("frm");
			
			comAjax.setUrl("<c:url value='/proc-find-email.do' />");
			comAjax.setCallback("fn_findEmailCallback");
			comAjax.ajax();
			
		}
		
		// 사용자 정보로 Email Address 가져오기 Callback
		function fn_findEmailCallback(data) {
			
			const total = data.total ;

			if (total == 0) {
				fn_openLayerPop("noEmail") ;
				return ;
			}
			
			let body = $("div").children('.tx-blue'); 
			
			body.empty();
			
			let str = "";
			
			$.each(data.list, function(key, values){
				str += `\${values.user_id}<br>`;
			});
			
			body.append(str);
			
			fn_openLayerPop("findEmail") ;
			
		}
		
		// Layer Popup Open
		function fn_openLayerPop(pType) {
			switch (pType) {
			case "findEmail":
				$('#findEmail').show();
				break ;
			case "noEmail":
				$('#noEmail').show();
				break ;
			default:
				break ;
			}
		}
		
		// Layer Popup Close
		function fn_closeLayerPop(pType) {
			switch (pType) {
				case "findEmail":
					$('#findEmail').hide();
					location.href = "/login.do" ;
					break ;
				case "noEmail":
					$('#noEmail').hide();
					break ;
				default:
					break ;
			}
		}
		
	</script>
	
	<%@ include file="/WEB-INF/include/include-footer.jspf" %>

</html>