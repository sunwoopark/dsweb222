<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	    <title>대전시 관리자</title>
	    
		<!-- UI Definition -->
		<!--  CSS -->
		<link rel="stylesheet" href="/common/css/jquery-ui.css">
	    <link rel="stylesheet" href="/common/css/style.css">
		
		<!-- jQuery -->
		<script src="/common/js/jquery-3.2.1.min.js"></script>
	    <script src="/common/js/jquery-ui.js"></script>
	    <script src="/common/js/common.js"></script>
    	
    	<!-- Biz Process -->
	    <script src="/js/custom.js"></script>
	</head>
	<body>
		<p id="accessibility"><a href="#container">본문바로가기</a></p>   
		<div id="wrap">
		    <header id="header">
		        <div class="static">
		            <h1><a href="#">대전시</a></h1>
		            <div>
		               <h2>비밀번호 찾기</h2>
		            </div>
		        </div>      
		    </header>
		    
	    	<div id="container">
	    		<div class="contents login-content">
	    			<div class="login-info-find pass-find">
	    				<label for="u-email">ID</label>
               			<input type="text" id="user_emad" name="user_emad" class="ipt">
		                <label for="u-name">이름</label>
		                <input type="text" id="user_nm" name="user_nm" class="ipt">
		                <button type="button" class="btn-lg blue" onclick="fn_findPassword() ; return false ;">비밀번호 찾기</button>
	    			</div>
	    		</div>
	    	</div>
		    
		    <footer id="footer">
<address>(우)35238 대전광역시 서구 둔산서로 100(둔산동)</address><span class="tel">대표전화 : 042)288-2114/ 팩스 : 042)288-5900</span>
            <span class="copy">Copyright (C) 2014 Seo-gu Office. All rights reserved.</span>			</footer>
		</div>
		<!-- #wrap -->
		
		<script type="text/javascript">
		
			// 난수로 생성한 비밀번호로 변경 후 가입한 이메일로 이메일과 Login Page URL 전송 
			function fn_findPassword() {
				
				if (!$("#user_emad").val()) {
					fn_openType2LayerPop("user_emad")
					return ;
				}
				
				if (!$("#user_nm").val()) {
					fn_openType2LayerPop("user_nm")
					return ;
				}
				
				$.ajax({
					url : "/proc-find-password.do",
					type : "POST",
					data : {
						user_id : $("#user_emad").val(),
						user_nm : $("#user_nm").val()
					},
					dataType : "json",
					success : function(data){
						if (data.result.result_code == "S"){
							// Select한 결과가 있을 경우
							fn_openType2LayerPop("sendMail") ;
							return ;

						} else if (data.result.result_code == "N"){
							// 계정정보 없을 경우
							fn_openType2LayerPop("noAccountInfo") ;
							return ;
						} else {
							// 처리 실패
							fn_openType1LayerPop("fail", data.result.result_msg) ;
							return ;
						}
					},
					error : function(data) {}
				});
				
			}
			
			// Type1 Layer Popup 공통 처리
			// Message가 한 줄이고 큰 글자인 경우
			function fn_openType1LayerPop(pType, pMsg) {
				let msg1 ;
				
				switch (pType) {
				case "fail":
					msg1 = pMsg ;
					break;
				default:
				}
				
				$("#type1Msg").html(msg1) ;
				$("#layerPopType1").show() ;
				
			}
			
			// Type2 Layer Popup 공통 처리
			// Message가 두 줄인 경우
			function fn_openType2LayerPop(pType) {
				
				let msg1 ;
				let msg2 ;
				
				switch (pType) {
				case "user_emad":
					msg1 = "이메일을 입력하세요!" ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break ;
				case "user_nm":
					msg1 = "이름을 입력하세요!" ;
					msg2 = "다시 확인하여 주시기 바랍니다." ;
					break ;
				case "noAccountInfo":
					msg1 = "해당정보로 가입한 계정은 없습니다." ;
					msg2 = "메일을 확인하여 주시기 바랍니다." ;
					break;
				case "sendMail":
					msg1 = "해당정보로 가입된 E-mail 로 인증메일을 보냈습니다." ;
					msg2 = "확인하여 주시기 바랍니다." ;
					break;
				case "fail":
					msg1 = "해당정보로 가입된 E-mail 로 인증메일을 보냈습니다." ;
					msg2 = "확인하여 주시기 바랍니다." ;
					break;
				default:
				}
				
				$("#type2Msg1").html(msg1) ;
				$("#type2Msg2").html(msg2) ;
				
				$("#layerPopType2").show() ;
				
			}
		
		</script>
		
		<%@ include file="/WEB-INF/include/include-layerpopup.jspf" %>
		
	</body>
</html>