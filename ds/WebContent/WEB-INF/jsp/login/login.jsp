<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
	<head>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	    <title>대전시 관리자</title>
	    
		<!-- UI Definition -->
		<!--  CSS -->
		<link rel="stylesheet" href="/common/css/jquery-ui.css">
	    <link rel="stylesheet" href="/common/css/style.css">
		
		<!-- jQuery -->
		<script src="/common/js/jquery-3.2.1.min.js"></script>
	    <script src="/common/js/jquery-ui.js"></script>
	    <script src="/common/js/common.js"></script>
    	
    	<!-- Biz Process -->
	    <script src="/js/custom.js"></script>
		<script src="/js/jquery.fileDownload.js"></script>
	</head>
	<body>
		<p id="accessibility"><a href="#container">본문바로가기</a></p>   
		<div id="wrap">
		    <header id="header">
		        <div class="static">
		            <h1><a href="javaScript:void(0);">대전시</a></h1>
		            <div>
		               <h2>로그인</h2>
		            </div>
		        </div>      
		    </header>
		    
		    <div id="container">
		    	<div class="contents login-content">
		    		<div class="login-wrap">
		    			<div class="login-form-bx">
		    				<div class="login-form">
		    					<label for="u-email">ID</label>
		    					<input type="text" id="login-id" name="login-id" class="ipt" maxlength= "20" >
		    					<label for="u-pass">비밀번호</label>
		    					<input type="password" id="login-pass" name="login-pass" class="ipt" maxlength= "20">
		    					<button type="submit" class="btn-lg blue" onclick="fn_login() ; return false ;">로그인</button>
		    				</div>
		    			</div>
		    			<div class="btns-btm-center">
		    				<a href="javascript:void(0);" class="btn-lg black" onclick="location.href='/join.do'">회원가입</a>
		    				<a href="javascript:void(0);" class="btn-lg" onclick="location.href='/find-email.do'">ID 찾기</a>
		    				<!-- <a href="javascript:void(0);" class="btn-lg" onclick="location.href='/find-password.do'">비밀번호 찾기</a> -->
		    				<a href="javascript:void(0);" class="btn-lg" onclick="fn_openLayerPop('findPasswd'); return false;">비밀번호 찾기</a>
		    				<a href="javascript:void(0);" class="btn-lg indigo" onClick="fn_menuDownload(); return false;">메뉴얼 다운로드</a> 
		    			</div>
		    		</div>
		    	</div>
		    	<!-- //contents -->
		    </div>
		    <!-- //container -->
		
			<footer id="footer">
<address>(우)35238 대전광역시 서구 둔산서로 100(둔산동)</address><span class="tel">대표전화 : 042)288-2114/ 팩스 : 042)288-5900</span>
            <span class="copy">Copyright (C) 2014 Seo-gu Office. All rights reserved.</span>			</footer>
		</div>
		<!-- #wrap -->
		
		<!-- Layer Popup -->
		<!-- 비밀번호 변경 필요 -->
		<div class="layer-pop" id="changePasswd">
			 <div class="alert-pop">
			 	<div class="alert-pop-head">
			 		<button type="button" class="alert-close">닫기</button>
			 	</div>
			 	<div class="alert-pop-body">
			 		<div class="msg">
			 			<strong class="tx2">비밀번호를 변경해 주세요.</strong>
			 		</div>
			 		<div class="btm">
			 			<div class="btm-btns">
			 				<button type="button" class="btn-md black" id="changePasswdConfirm">확인</button>
			 			</div>
			 		</div>
			 	</div>
			 </div>
		</div>
		
		<!-- 비밀번호 찾기 -->
		<div class="layer-pop" id="findPasswd">
			 <div class="alert-pop">
			 	<div class="alert-pop-head">
			 		<button type="button" class="alert-close">닫기</button>
			 	</div>
			 	<div class="alert-pop-body">
			 		<div class="msg">
			 			<strong class="tx2">관리자에게 문의하시기 바랍니다.</strong>
			 		</div>
			 		<div class="btm">
			 			<p class="tx-blue">관리자 이메일 :  chanbob79@korea.kr</p>
			 			<div class="btm-btns">
			 				<button type="button" class="btn-md black" onClick="fn_closeLayerPop('findPasswd'); return false;">확인</button>
			 			</div>
			 		</div>
			 	</div>
			 </div>
		</div>
		
		<!-- 미승인 -->
		<div class="layer-pop" id="apprvFail">
			<div class="alert-pop">
				<div class="alert-pop-head">
					<button type="button" class="alert-close">닫기</button>
				</div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong>승인되지 않은 계정입니다.</strong>
						<p>관리자에게 승인 요청 부탁드립니다.</p>
					</div>
					<div class="btm">
						<p class="tx-blue">관리자 이메일 :  chanbob79@korea.kr</p>
						<div class="btm-btns">
							<button type="button" class="btn-md black" onclick="fn_closeLayerPop('apprvFail') ; return false ;" >확인</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 미승인 -->
		
		<!-- Login 정보 오류 -->
		<div class="layer-pop" id="loginInfoFail">
			<div class="alert-pop">
				<div class="alert-pop-head">
					<button type="button" class="alert-close">닫기</button>
				</div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong>가입되지 않은 계정입니다.</strong>
						<p>다시 확인하여 주시기 바랍니다.</p>
					</div>
					<div class="btm">
						<p class="tx-blue">관리자 이메일 :  chanbob79@korea.kr</p>
						<div class="btm-btns">
							<button type="button" class="btn-md black" onclick="fn_closeLayerPop('loginInfoFail') ; return false ;" >확인</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Login 정보 오류 -->
		
		<!-- 권한 오류 -->
		<div class="layer-pop" id="athrFail">
			<div class="alert-pop">
				<div class="alert-pop-head">
					<button type="button" class="alert-close">닫기</button>
				</div>
				<div class="alert-pop-body">
					<div class="msg">
						<strong>권한이 없는 계정입니다.</strong>
            			<p>다시 확인하여 주시기 바랍니다.</p>
					</div>
					<div class="btm">
						<p class="tx-blue">관리자 이메일 :  chanbob79@korea.kr</p>
						<div class="btm-btns">
							<button type="button" class="btn-md black" onclick="fn_closeLayerPop('athrFail') ; return false ;" >확인</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 권한 오류 -->
		<form id="commonForm" name="commonForm"></form>
		<script type="text/javascript">
			"use strict" ;
			
			$(document).ready(function(){
				
				// 비밀번호 변경 요청 확인 button click
				$("#changePasswdConfirm").on("click",function() {
					$('#changePasswd').hide();
					location.href = "/change-password.do" ;
					return ;
				});
			
			});
			
			// 사용자 Login
			function fn_login() {
				
				// 필수 입력 Check
				if (!$("#login-id").val()) {
					alert("아이디를 입력해주세요.") ;
					$("#login-id").focus() ;
					return;
				}
				
				if (!$("#login-pass").val()) {
					alert("비밀번호를 입력해주세요.") ;
					$("#login-pass").focus() ;
					return ;
				}
				
				$.ajax({
					url : "/login/proc-login-info.do",
					type : "POST",
					data : {
						user_id : $("#login-id").val(),
						user_passwd : $("#login-pass").val()
					},
					dataType : "json",
					success : function(data){
						if (data.result.result_code == "S"){
							// Select한 결과가 있을 경우
							// 승인여부 Check
							if (data.result.aprv_yn == "0") {
								fn_openLayerPop("apprvFail") ;
								return ;
							}
							
							// 관리자로 로그인한 경우
							// 관리자 로그인 페이지로 이동
							if (data.result.admin_yn == "1") {
								location.href = "/login-sadm.do" ;
								location.href = "/table.do" ;
								return ;
							}
							
							// 비밀번호 초기화 후 Login한 경우
							if (data.result.passwd_init_yn == "1") {
								fn_openLayerPop("changePasswd") ;
								return ;
							}
							
							// 축산/의료/농업/관리자 권한이 없을 경우
							if (data.result.page_athr_cd != "STKRS" &&
									data.result.page_athr_cd != "FARMNG" &&	
									data.result.page_athr_cd != "MDCL" &&
									data.result.page_athr_cd != "ADMIN" &&
									data.result.page_athr_cd != "CMN"
									) {
								fn_openLayerPop("athrFail") ;
								return ;
							}
							
							switch(data.result.page_athr_cd) {
							case "STKRS":
								location.href = "/lvst/main.do" ;
								break;
							case "FARMNG":
								location.href = "/farm/list.do" ;
								break;
							case "MDCL":
								location.href = "/mdcl/main.do" ;
								break ;
							case "CMN":
								location.href = "/visual/population1-1.do";
								break;
							default:
								break;
							
							}
							return ;

						} else {
							// Select한 결과가 없을 경우
							fn_openLayerPop("loginInfoFail") ;
							return ;
						}
					},
					error : function(data) {}
				});
				
			}
			
			// Layer Popup Open
			function fn_openLayerPop(pType) {
				
				switch (pType) {
				case "changePasswd":
					$('#changePasswd').show();
					break ;
				case "loginInfoFail":
					$('#loginInfoFail').show();
					break ;
				case "apprvFail":
					$('#apprvFail').show();
					break ;
				case "athrFail":
					$('#athrFail').show();
					break ;
				case "findPasswd":
					$('#findPasswd').show();
					break;
				default:
					break ;
				}
				
			}
			
			// Login 실패 Layer Popup Close
			function fn_closeLayerPop(pType) {
				
				switch (pType) {
					case "loginInfoFail":
						$('#loginInfoFail').hide();
						break ;
					case "apprvFail":
						$('#apprvFail').hide();
						break ;
					case "athrFail":
						$('#athrFail').hide();
						break ;
					case "findPasswd":
						$('#findPasswd').hide();
						break ;
					default:
						break ;
				}
				
			}
			// 메뉴얼 다운로드
			function fn_menuDownload(){
				let comSubmit = new ComSubmit();
				comSubmit.setUrl("<c:url value='/login/menualDownload.do' />");
				comSubmit.submit();
				
			}
		</script>
		
	</body>
</html>