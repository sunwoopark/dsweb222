<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>


<div class="contents-top">
	<div class="lnbmenu">
		<a href="/visual/nightpark.do" class="btn-md off">화물차 밤샘주차</a> <a
			href="/visual/transit.do" class="btn-md on">화물차 운송사업자</a> <a
			href="/visual/neglect.do" class="btn-md off">무단방치차량 단속</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>화물차 운송사업자 현황</h3>
	</div>
	<div class="btn-nbd">
		<button class="btn-nb2 on" onclick="chart1()">개인화물</button>
		<button class="btn-nb2 off" onclick="chart2()">일반화물</button>
	</div>
</div>
<div class="section">
	<div class="grid-full" id="mapwrap"></div>
	<div class="h-group2">
		<div>
			<h3>행정동별 사업자 현황</h3>
		</div>
	</div>
	<div class="grid-full" id="bar"></div>
	<!-- 개인/일반 사업자 비율 숨김
	<div class="h-group2">
		<div>
			<h3>개인/일반 사업자 비율</h3>
		</div>
		<div>
			<span class="tit">행정동</span> <select id="dong" name="dong"
				onchange="chart4();">
				<c:forEach var="dong" items="${dong}" varStatus="status">
					<option value="${dong.dong }">${dong.dong }</option>
				</c:forEach>
			</select>
		</div>
	</div>
	<div class="grid-twin">
		<div id="pie"></div>
	</div> -->

	<!-- 월별 화물차 밤샘주차단속현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>행정동별 화물차운송사업자 현황</h3>
		</div>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 25%;">
				<col style="width: 25%;">
				<col style="width: 25%;">
				<col style="width: 25%;">
			</colgroup>
			<thead>
				<tr>
					<th>행정동</th>
					<th>사업자현황</th>
					<th>행정동</th>
					<th>사업자현황</th>
				</tr>
			</thead>

			<tbody>
				<c:set var="i" value="0" />
				<c:set var="j" value="2" />
				<c:forEach items="${list3 }" var="list3">
					<c:if test="${i%j == 0 }">
						<tr>
					</c:if>
					<td>${list3.dong_nm }</td>
					<td><fmt:formatNumber value="${list3.cnt}" pattern="#,###" /></td>
					<c:if test="${i%j == j-1 }">
						</tr>
					</c:if>
					<c:set var="i" value="${i+1 }" />
				</c:forEach>
				<td>-</td>
				<td>-</td>
			</tbody>
		</table>
	</div>
	<!-- 월별 화물차 밤샘주차단속현황 종료 -->

	<!-- 화물차 운송유형별 비중 시작 -->
	<div class="h-group2">
		<div>
			<h3>화물차 운송유형별 비중</h3>
		</div>
	</div>
	<div class="grid-full">
		<div>
			<table border="1" style="text-align: center;">
				<colgroup>
					<col style="width: 70%;">
					<col style="width: 30%;">
				</colgroup>
				<c:set var="total" value="0" />
				<c:forEach items="${list4 }" var="list4">
					<c:set var="total" value="${total + list4.cargo_cnt}" />
				</c:forEach>
				<tr class="cargo_type">
					<c:forEach items="${list4 }" var="list4">
						<th>${list4.cargo_cate} : <fmt:formatNumber
								value="${(list4.cargo_cnt / total) * 100}" pattern="0.0" /> %</th>
					</c:forEach>
				</tr>

			</table>
		</div>

	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 50%;">
				<col style="width: 50%;">
			</colgroup>
			<thead>
				<tr>
					<th>개인화물(개)</th>
					<th>일반화물(개)</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${list4 }" var="list4">
					<td><fmt:formatNumber value="${list4.cargo_cnt}"
							pattern="#,###" /></td>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<!-- 화물차 운송유형별 비중 종료 -->

</div>


<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="../visual/common/js/Chart.min.js"></script>
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=cc46f2d48285e645648b9d4a912d6346"></script>
<script>
Chart.defaults.global.legend.labels.usePointStyle = true;
chart1();
chart3();
chart4();
$(function(){
    $('.btn-nbd>button').on('click',function(){
        $(this).parent().find('button').removeClass('on')
        $(this).parent().find('button').addClass('off')
        $(this).removeClass('off')
        $(this).addClass('on')
    });
});
function chart1(){
$("#mapwrap").html('<div style="width:1050px;height:520px;"><div id="map" style="width:1050px;height:500px;"></div></div>');
var year = $('#year').val();
var regexp = /\B(?=(\d{3})+(?!\d))/g;
$.ajax({
	url:'/visual/proc-transit.do',
	type:'post',
	data:{},
	success:function(data){

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
	center: new kakao.maps.LatLng(36.3556250, 127.3837457), // 지도의 중심좌표
    level: 5 // 지도의 확대 레벨
    };
var imageSrc = '../visual/images/red_pin.png', // 마커이미지의 주소입니다    
imageSize = new kakao.maps.Size(8, 8), // 마커이미지의 크기입니다
imageOption = {offset: new kakao.maps.Point(8, 8)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
var markerArray = new Array();
//var markerImage = new kakao.maps.MarkerImage('../../../images/visual/blue_pin.png', imageSize, imageOption) // 마커가 표시될 위치입니다
//마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다

var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
markerPosition = new kakao.maps.LatLng(37.54699, 127.09598); // 마커가 표시될 위치입니다

var map = new kakao.maps.Map(mapContainer, mapOption),
    infowindow = new kakao.maps.InfoWindow({removable: true});
var cont = new Array();
var llng = new Array();
$.each(data.list, function(key, values){
	if(`\${values.bsns_tp}` == '개인화물'){
	cont.push("<div style='font-size:10pt; padding:5px 10px 0px;text-align: center; vertical-align: middle; white-space:nowrap;'>"+ `\${values.rd_addr}`+"</div>");
	llng.push(new kakao.maps.LatLng(`\${values.lo}` , `\${values.la}`));
	}
})

var positions = [];
	for(var i = 0; i < llng.length; i ++){
		positions.push(
	    {
	        content: cont[i], 
	        latlng: llng[i]
	    },
	    )
	}


// 마커 이미지의 이미지 주소입니다
for (var i = 0; i < positions.length; i ++) {
//마커 이미지의 이미지 크기 입니다

    var marker = new kakao.maps.Marker({
        map: map, // 마커를 표시할 지도
        position: positions[i].latlng, // 마커를 표시할 위치
        image: markerImage
    });
    
    var infowindow = new kakao.maps.InfoWindow({
        content: positions[i].content // 인포윈도우에 표시할 내용
    });
    
    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
 
}

function makeOverListener(map, marker, infowindow) {
    return function() {
        infowindow.open(map, marker);
    };
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
function makeOutListener(infowindow) {
    return function() {
        infowindow.close();
    };
}

	}, error: function(){
		alert("aaa");
	}
})
}
function chart2(){
	$("#mapwrap").html('<div style="width:1050px;height:520px;"><div id="map" style="width:1050px;height:500px;"></div></div>');
	var year = $('#year').val();
	var regexp = /\B(?=(\d{3})+(?!\d))/g;
	$.ajax({
		url:'/visual/proc-transit.do',
		type:'post',
		data:{},
		success:function(data){

	var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
	    mapOption = { 
		center: new kakao.maps.LatLng(36.3556250, 127.3837457), // 지도의 중심좌표
	    level: 5 // 지도의 확대 레벨
	    };
	var imageSrc = '../visual/images/red_pin.png', // 마커이미지의 주소입니다    
	imageSize = new kakao.maps.Size(8, 8), // 마커이미지의 크기입니다
	imageOption = {offset: new kakao.maps.Point(8, 8)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
	var markerArray = new Array();
	//var markerImage = new kakao.maps.MarkerImage('../../../images/visual/blue_pin.png', imageSize, imageOption) // 마커가 표시될 위치입니다
	//마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다

	var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
	markerPosition = new kakao.maps.LatLng(37.54699, 127.09598); // 마커가 표시될 위치입니다

	var map = new kakao.maps.Map(mapContainer, mapOption),
	    infowindow = new kakao.maps.InfoWindow({removable: true});
	var cont = new Array();
	var llng = new Array();
	$.each(data.list, function(key, values){
		if(`\${values.bsns_tp}` == '일반화물'){
		cont.push("<div style='font-size:10pt; padding:5px 10px 0px;text-align: center; vertical-align: middle; white-space:nowrap;'>"+ `\${values.rd_addr}`+"</div>");
		llng.push(new kakao.maps.LatLng(`\${values.lo}` , `\${values.la}`));
		}
	})

	var positions = [];
		for(var i = 0; i < llng.length; i ++){
			positions.push(
		    {
		        content: cont[i], 
		        latlng: llng[i]
		    },
		    )
		}


	// 마커 이미지의 이미지 주소입니다
	for (var i = 0; i < positions.length; i ++) {
	//마커 이미지의 이미지 크기 입니다

	    var marker = new kakao.maps.Marker({
	        map: map, // 마커를 표시할 지도
	        position: positions[i].latlng, // 마커를 표시할 위치
	        image: markerImage
	    });
	    
	    var infowindow = new kakao.maps.InfoWindow({
	        content: positions[i].content // 인포윈도우에 표시할 내용
	    });
	    
	    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
	    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
	 
	}

	function makeOverListener(map, marker, infowindow) {
	    return function() {
	        infowindow.open(map, marker);
	    };
	}

	// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
	function makeOutListener(infowindow) {
	    return function() {
	        infowindow.close();
	    };
	}

		}, error: function(){
			alert("aaa");
		}
	})
	}
function chart3(){
	var year = $('#year').val();
	var val = new Array();
	var label = new Array();
	$("#bar").html("<canvas id='Chart3' height='80px;'></canvas>");
	$.ajax({
		url:'/visual/proc-transit.do',
		type:'post',
		data:{},
		success:function(data){
			var ctx = "";
			$.each(data.list1, function(key, values){
					label.push(`\${values.dong}`);
					val.push(`\${values.cnt}`);
				
			});
				var mydata = {
				        labels: label,
				        datasets: [{
				        	label: '행정동별',
					        data: val,
					        backgroundColor: "#2914FF",
					    	hoverBackgroundColor: "#2914FF"
					      }
					    ]
				    };
				
				var op = {
					    title: {
						      display: false,
						      text: ''
						    },
						legend: {
						
							position: 'bottom'
						},
						tooltips: { mode: 'index',
							callbacks: {label: function(tooltipItem, data) {
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								  
					              if (label) {
					                  label += ': ';
					              }
						  		  let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						  		  value = value.toString().replace(regexp, ',');
						  		  return label + value;
						  		}
						    }},
						scales: {
						      yAxes: [{
						    	  ticks: {
						    		  beginAtZero: true,
						    		  userCallback: function(value, index, values) {
						    			  value = value.toString().replace(regexp, ',');
						    		    return value;
						    		  },
						    		}
						      }],  
						      xAxes: [{
						      }],
						    }
						  };
			
				var ctx = document.getElementById("Chart3").getContext("2d");
				var myChart = new Chart(ctx, {
				    type: 'bar',
				    data: mydata,
				    options: op
				});
		}, error: function(){
			alert("aaa");
		}
	})
}
function chart4(){
	var dong = $('#dong').val();
	var val = new Array();
	var label = new Array();
	$("#pie").html("<canvas id='Chart4' height='200px;'></canvas>");
	$.ajax({
		url:'/visual/proc-transit.do',
		type:'post',
		data:{dong:dong},
		success:function(data){
			var ctx = "";
			$.each(data.list2, function(key, values){
					label.push(`\${values.bsns_tp}`);
					val.push(`\${values.cnt}`);
				
			});
				var mydata = {
				        labels: label,
				        datasets: [{
					        data: val,
					        backgroundColor: ["#2914FF","#6322E6"],
					    	hoverBackgroundColor: ["#2914FF","#6322E6"]
					      }
					    ]
				    };
				
				var op = {
					    title: {
						      display: false,
						      text: ''
						    },
							legend: {
								
								position: 'bottom'
							},
						tooltips: { mode: 'index',
							callbacks: {label: function(tooltipItem, data) {
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								  
					              if (label) {
					                  label += ': ';
					              }
						  		  let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						  		  value = value.toString().replace(regexp, ',');
						  		  return label + value;
						  		}
						    }}
						  };
			
				var ctx = document.getElementById("Chart4").getContext("2d");
				var myChart = new Chart(ctx, {
				    type: 'pie',
				    data: mydata,
				    options: op
				});
		}, error: function(){
			alert("aaa");
		}
	})
}
</script>

<script>
function chart5() {
	var cargo_cate = new Array();
	var cargo_cnt = new Array();
	
	<c:forEach items="${list4}" var="list4">
		cargo_cate.push('${list4.cargo_cate}');
		cargo_cnt.push(${list4.cargo_cnt});
	</c:forEach>
	
	var mydata = {
        labels: " " ,
        datasets: [{
            label: '개인화물',
            data: [1683],
            borderColor: "blue",
            backgroundColor: "blue",
            hoverBackgroundColor: "blue",
            barWidth:4,
           	fill: false
        },{
        	label: '일반화물',
	        data: [194],
	        borderColor: "green",
	        backgroundColor: "green",
	        hoverBackgroundColor: "green",
	        pointBorderWidth: 1,
	        barWidth:4,
	       	fill: false
    	}]
    };
    var op = { 		
        legend: {
        	defaultFontSize	 : 30
        },
        tooltips: {
        	enabled: false
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
            duration: 1,
            onComplete: function() {
            	var chartInstance = this.chart,
				ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function(dataset) {
                    for (var i = 0; i < dataset.data.length; i++) {
                        for (var key in dataset._meta) {
                            var model = dataset._meta[key].data[i]._model;
                            ctx.fillText(dataset.data[i].toLocaleString(), model.x, model.y - 13);
                        }
                    }
                });
            }
        },
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 300, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                     
                 },
           			 stacked: true 
             }],
             xAxes: [{ stacked: true
             }],
        }
    };

	// chart5 설정 최종
    var ctx = document.getElementById("Chart5").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: mydata,
        options: op
    });
}

chart5();
</script>