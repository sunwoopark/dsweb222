<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>

<div class="contents-top">
	<div class="lnbmenu">
		<a href="/visual/nightpark.do" class="btn-md off">화물차 밤샘주차</a> <a
			href="/visual/transit.do" class="btn-md off">화물차 운송사업자</a> <a
			href="/visual/neglect.do" class="btn-md on">무단방치차량 단속</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>무단방치차량 단속</h3>
	</div>
	<div>
		<span class="tit">연도</span> <select id="year" name="year"
			onchange="chart1();">
			<c:forEach var="year" items="${year}" varStatus="status">
				<option value="${year.std_date }">${year.std_date }</option>
			</c:forEach>
		</select>
	</div>
</div>
<div class="section">
	<div class="grid-full" id="mapwrap"></div>

	<!-- 연도별 월별 무단방치차량 단속 현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>연도별 월별 무단방치차량 단속 현황</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart3' height='120px;'></canvas>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 25%;">
				<col style="width: 25%;">
				<col style="width: 25%;">
				<col style="width: 25%;">
			</colgroup>
			<thead>
				<tr>
					<th>월</th>
					<th>2018년(건)</th>
					<th>2019년(건)</th>
					<th>2020년(건)</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />
			<c:set var="sum_two" value="0" />
			<c:set var="sum_three" value="0" />

			<tbody>
				<c:forEach items="${list3_1 }" var="list3_1" varStatus="status">
					<tr>
						<td>${list3_1.std_mm }월</td>
						<td><fmt:formatNumber value="${list3_1.car_cnt }"
								pattern="#,###" /></td>
						<td><fmt:formatNumber
								value="${list3_2[status.index].car_cnt}" pattern="#,###" /></td>
						<td><c:if test="${list3_3[status.index].car_cnt != null}">
								<fmt:formatNumber value="${list3_3[status.index].car_cnt}"
									pattern="#,###" />
							</c:if> <c:if test="${list3_3[status.index].car_cnt == null}">-</c:if></td>
					</tr>
					<c:set var="sum_one" value="${sum_one + list3_1.car_cnt}" />
					<c:set var="sum_two"
						value="${sum_two + list3_2[status.index].car_cnt}" />
					<c:set var="sum_three"
						value="${sum_three + list3_3[status.index].car_cnt}" />
				</c:forEach>
				<tr>
					<td>총합계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_two}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_three}" pattern="#,###" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- 행정동별 소화전 개수 끝 -->

	<!-- 행정동별무단방치차량현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>행정동별 무단방치차량 현황</h3>
		</div>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 25%;">
				<col style="width: 25%;">
				<col style="width: 25%;">
				<col style="width: 25%;">
			</colgroup>
			<thead>
				<tr>
					<th>행정동</th>
					<th>이륜차</th>
					<th>자동차</th>
					<th>총합계</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />
			<c:set var="sum_two" value="0" />
			<c:set var="sum_three" value="0" />

			<tbody>
				<c:forEach items="${list4_1 }" var="list4_1" varStatus="status">
					<tr>
						<td>${list4_1.dong_nm }</td>
						<td><fmt:formatNumber value="${list4_1.car_cnt }"
								pattern="#,###" /></td>
						<td><fmt:formatNumber
								value="${list4_2[status.index].car_cnt}" pattern="#,###" /></td>
						<td><fmt:formatNumber
								value="${list4_3[status.index].car_cnt}" pattern="#,###" /></td>
					</tr>
					<c:set var="sum_one" value="${sum_one + list4_1.car_cnt}" />
					<c:set var="sum_two"
						value="${sum_two + list4_2[status.index].car_cnt}" />
					<c:set var="sum_three"
						value="${sum_three + list4_3[status.index].car_cnt}" />
				</c:forEach>
				<tr>
					<td>총합계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_two}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_three}" pattern="#,###" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- 행정동별 무단방치차량 현황 끝 -->


</div>


<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="../visual/common/js/Chart.min.js"></script>
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=cc46f2d48285e645648b9d4a912d6346"></script>
<script>
Chart.defaults.global.legend.labels.usePointStyle = true;
chart1();
$(function(){
    $('.btn-nbd>button').on('click',function(){
        $(this).parent().find('button').removeClass('on')
        $(this).parent().find('button').addClass('off')
        $(this).removeClass('off')
        $(this).addClass('on')
    });
});
function chart1(){
$("#mapwrap").html('<div style="width:1050px;height:520px;"><div id="map" style="width:1050px;height:500px;"></div></div>');
var year = $('#year').val();
var regexp = /\B(?=(\d{3})+(?!\d))/g;
$.ajax({
	url:'/visual/proc-neglect.do',
	type:'post',
	data:{year:year},
	success:function(data){

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
	center: new kakao.maps.LatLng(36.3556250, 127.3837457), // 지도의 중심좌표
    level: 5 // 지도의 확대 레벨
    };
var imageSrc = '../visual/images/red_pin.png', // 마커이미지의 주소입니다    
imageSize = new kakao.maps.Size(8, 8), // 마커이미지의 크기입니다
imageOption = {offset: new kakao.maps.Point(8, 8)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.

var markerArray = new Array();
//var markerImage = new kakao.maps.MarkerImage('../../../images/visual/blue_pin.png', imageSize, imageOption) // 마커가 표시될 위치입니다
//마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다

var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
markerPosition = new kakao.maps.LatLng(37.54699, 127.09598); // 마커가 표시될 위치입니다

var map = new kakao.maps.Map(mapContainer, mapOption),
    infowindow = new kakao.maps.InfoWindow({removable: true});
var cont = new Array();
var llng = new Array();
$.each(data.list, function(key, values){
	cont.push("<div style='font-size:10pt; padding:5px 10px 0px;text-align: center; vertical-align: middle; white-space:nowrap;'>"+ `\${values.tckt_addr}`+"</div>");
	llng.push(new kakao.maps.LatLng(`\${values.lo}` , `\${values.la}`));
})

var positions = [];
	for(var i = 0; i < llng.length; i ++){
		positions.push(
	    {
	        content: cont[i], 
	        latlng: llng[i]
	    },
	    )
	}


// 마커 이미지의 이미지 주소입니다
for (var i = 0; i < positions.length; i ++) {
//마커 이미지의 이미지 크기 입니다

    var marker = new kakao.maps.Marker({
        map: map, // 마커를 표시할 지도
        position: positions[i].latlng, // 마커를 표시할 위치
        image: markerImage
    });
    
    var infowindow = new kakao.maps.InfoWindow({
        content: positions[i].content // 인포윈도우에 표시할 내용
    });
    
    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
 
}

function makeOverListener(map, marker, infowindow) {
    return function() {
        infowindow.open(map, marker);
    };
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
function makeOutListener(infowindow) {
    return function() {
        infowindow.close();
    };
}

	}, error: function(){
		alert("aaa");
	}
})
}
</script>

<script>
function chart3() {

	var std_mm = new Array();
	var car_cnt = new Array();
	var avg = new Array();

	<c:forEach items="${list33}" var="list33">
		std_mm.push('${list33.std_mm}');
		car_cnt.push(${list33.car_cnt});
	</c:forEach>
	
	/* var yScale = chartInstance.scales["y-axis-0"];
    var canvas = chartInstance.chart;
    var ctx = canvas.ctx;
    var index;
    var line;
    var style;
	if (chartInstance.options.horizontalLine) {
        for (index = 0; index < chartInstance.options.horizontalLine.length; index++) {
            line = chartInstance.options.horizontalLine[index];
            if (!line.style) {
                style = "rgba(169,169,169, .6)";
            } else {
                style = line.style;
            }
            if (line.y) {
                yValue = yScale.getPixelForValue(line.y);
            } else {
                yValue = 0;
            }
            ctx.lineWidth = 3;
            if (yValue) {
                ctx.beginPath();
                ctx.moveTo(0, yValue);
                ctx.lineTo(canvas.width, yValue);
                ctx.strokeStyle = style;
                ctx.stroke();
            }
            if (line.text) {
                ctx.fillStyle = style;
                ctx.fillText(line.text, 0, yValue + ctx.lineWidth);
            }
        }
        return;
    }; */

    var mydata = {
        labels: std_mm ,
        datasets: [{
            label: '2018년',
            data: car_cnt,
            backgroundColor: "navy",
            hoverBackgroundColor: "navy"
        }]
    };
    var op = { 		
        legend: {
        	display: false,
            position: 'top',
           	onClick: null	
        },
        tooltips: {
        	enabled: false
            /* mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            } */ 
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 10, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        },
        horizontalLine: [{
            "y": 82,
            "style": "rgba(255, 0, 0, .4)",
            "text": "max"
        }, {
            "y": 50,
            "style": "#00ffff",
            "text": "min"
        }]
    };

	// chart3 설정 최종
    var ctx = document.getElementById("Chart3").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}

chart3();

</script>