<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>
<div class="contents-top">
	<div>
		<a href="/visual/fine.do" class="btn-md on">과태료 현황</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>행정동별 대상 건물 수</h3>
	</div>
</div>
<div class="section">
	<div class="grid-full">
		<canvas id='Chart1' height='120px;'></canvas>
	</div>
	<!-- 행정동별 일평균 유동인구 숨김
      <div class="h-group">
         <h3>행정동별 일평균 유동인구</h3>
     </div>
     <div class="grid-full">
     	<canvas id='Chart2' height='120px;'></canvas>
     </div> 
     -->

	<!-- 행정동별 평균 부과금액 및 대상건물수 현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>행정동별 평균 부과금액 및 대상건물수 현황</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart3' height='120px;'></canvas>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<!-- 			<colgroup>
				<col style="width: 10%;">
				<col style="width: 10%;">
				<col style="width: 10%;">
				<col style="width: 10%;">
				<col style="width: 10%;">
				<col style="width: 10%;">
				<col style="width: 10%;">
			</colgroup> -->
			<thead>
				<tr>
					<th rowspan="2">행정동</th>
					<th colspan="2">대상건물 수</th>
					<th colspan="2">일평균 유동인구 수</th>
					<th colspan="2">평균 부과금액</th>
				</tr>
				<tr>
					<th>사례수(개)</th>
					<th>비중(%)</th>
					<th>사례수(개)</th>
					<th>비중(%)</th>
					<th>사례금액(원)</th>
					<th>비중(%)</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />
			<c:set var="sum_two" value="0" />
			<c:set var="sum_three" value="0" />
			<c:set var="sum_four" value="0" />
			<c:set var="sum_five" value="0" />
			<c:set var="sum_six" value="0" />

			<c:forEach items="${list3 }" var="list3">
				<c:set var="sum_one" value="${sum_one + list3.buil_cnt}" />
				<c:set var="sum_two" value="${sum_two + list3.buil_cnt}" />
				<c:set var="sum_three" value="${sum_three + list3.popul_cnt}" />
				<c:set var="sum_four" value="${sum_four + list3.popul_cnt}" />
				<c:set var="sum_five" value="${sum_five + list3.fine_cnt}" />
				<c:set var="sum_six" value="${sum_six + list3.fine_cnt}" />
			</c:forEach>

			<tbody>
				<c:forEach items="${list3 }" var="list3">
					<tr>
						<td>${list3.dong_nm }</td>
						<td><fmt:formatNumber value="${list3.buil_cnt }" /></td>
						<td><fmt:formatNumber
								value="${(list3.buil_cnt / sum_one) * 100}" pattern="0" /></td>
						<td><fmt:formatNumber value="${list3.popul_cnt }" /></td>
						<td><fmt:formatNumber
								value="${(list3.popul_cnt / sum_three) * 100}" pattern="0" /></td>
						<td><fmt:formatNumber value="${list3.fine_cnt }" /></td>
						<td><fmt:formatNumber
								value="${(list3.fine_cnt / sum_six) * 100}" pattern="0" /></td>
					</tr>

				</c:forEach>
				<tr>
					<td>합 계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="100" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_three}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="100" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_five}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="100" pattern="#,###" /></td>

				</tr>
			</tbody>
		</table>
	</div>
	<!-- 행정동별 평균 부과금액 및 대상건물수 현황 종료 -->

</div>
<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="../visual/common/js/Chart.min.js"></script>
<script>
Chart.defaults.global.legend.labels.usePointStyle = true;
chart1();


function chart1(){
	var inner = new Array();
	var label = new Array();
	var pop = new Array();
	<c:forEach items="${list}" var="list" >
			inner.push(${list.tower});
			label.push('${list.hang}');
			pop.push(${list.pop});
	</c:forEach>
	
	var mydata = {
	        labels: label,
	        
	        datasets: [{
	        	label: '대상건물수 ',
		        data: inner,
		        backgroundColor: "#3F45E8",
		    	hoverBackgroundColor: "blue"
		      }
		    ]
	    };
	
	var op = {
			legend: {
				position: 'bottom'
			},
			tooltips: { mode: 'index',
				callbacks: {label: function(tooltipItem, data) {
					var label = data.datasets[tooltipItem.datasetIndex].label || '';
					  
		              if (label) {
		                  label += ': ';
		              }
			  		  let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
			  		  value = value.toString().replace(regexp, ',');
			  		  return label + value;
			  		}
			    }},
			scales: {
			      yAxes: [{
			    	  ticks: {
			    		  beginAtZero: true,
			    		  userCallback: function(value, index, values) {
			    			  value = value.toString().replace(regexp, ',');
			    		    return value;
			    		  },
			    		}
			      }],  
			      xAxes: [{
			      }],
			    }
			  };

	var ctx = document.getElementById("Chart1").getContext("2d");
	var myChart = new Chart(ctx, {
	    type: 'bar',
	    data: mydata,
	    options: op
	});
	
	var mydata2 = {
			labels: label,
	  		datasets: [{
		    	type: 'line',
		    	data: pop,
		    	borderColor: "#1BF518",
		        fill: false,
		        lineTension: 0,
		        label: '월별',
		        pointBackgroundColor: "#1BF518"
		    }],
	};
	var op2 = {
		legend: {
			display: false,
			position: 'bottom'
		},
		responsive: true,
	    scales: {
	      yAxes: [{
	    	  ticks: {
	    		  stepSize: 500,
	    		  userCallback: function(value, index, values) {
	      		    value = value.toString().replace(regexp, ',');
	      		    return value;
	      		  }
	    	  }
	      }],  
	      xAxes: [{
	      }],
	    },
	    title: {
	    	display: true,
	    	text: '일평균 생활인구',
	    	fontSize: 12
	    },
	    tooltips:{
	    	callbacks: {label: function(tooltipItem, data) {
	  		  let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
	  		  value = value.toString().replace(regexp, ',');
	  		  return value;
	  		}
	    }
	  }
	  }
	var ctx2 = document.getElementById("Chart2").getContext("2d");
	var myChart2 = new Chart(ctx2, {
	    data: mydata2,
	    options: op2
	});

}
</script>

<script>

function chart3() {
	var dong_nm = new Array(); 
	var buil_cnt = new Array(); 
	var popul_cnt = new Array(); 
	var fine_cnt = new Array(); 

	<c:forEach items="${list3}" var="list3">
		dong_nm.push('${list3.dong_nm}');
		buil_cnt.push(${list3.buil_cnt});
		popul_cnt.push(${list3.popul_cnt});
		fine_cnt.push(${list3.fine_cnt});
	</c:forEach>


	var mydata = {
        labels: dong_nm ,
        datasets: [{
        	type: 'line',
            label: '평균 부과금액',
            data: fine_cnt,
            lineTension: '0', // 꺽은선
            borderColor: "green",
            backgroundColor: "green",
            hoverBackgroundColor: "green",
            yAxisID: 'A',
          	fill: false
        } , {
            label: '대상건물수',
            data: buil_cnt,
            backgroundColor: "navy",
            hoverBackgroundColor: "navy",
            yAxisID: 'B',
            	
        }]
    };
    var op = { 		
        legend: {
            position: 'top'
        },
        tooltips: {
        	enabled: false
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
            duration: 1,
            onComplete: function() {
            	var chartInstance = this.chart,
				ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function(dataset) {
                    for (var i = 0; i < dataset.data.length; i++) {
                        for (var key in dataset._meta) {
                            var model = dataset._meta[key].data[i]._model;
                            ctx.fillText(dataset.data[i].toLocaleString(), model.x, model.y - 13);
                        }
                    }
                });
            }
        },
        scales: {
            yAxes: [{
            	id: 'A',
                type: 'linear',
                position: 'left',
                ticks: {
                	 suggestedMax: 0, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
            }, {
            	id: 'B',
                type: 'linear',
                position: 'right',
                ticks: {
                	 max: 400,
                     min: 0,
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart3 설정 최종
    var ctx = document.getElementById("Chart3").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}

chart3();
</script>