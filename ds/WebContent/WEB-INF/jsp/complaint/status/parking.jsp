<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>
<div class="contents-top">
	<div>
		<a href="/visual/traffic.do" class="btn-md ">교통 민원 분석</a> <a
			href="/visual/parking.do" class="btn-md on">주차 민원 분석</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>주차 민원 키워드 분석</h3>
	</div>
	<div>
		<span class="tit">연도</span> <select id="year" name="year"
			onchange="chart1();chart2();">
			<c:forEach var="year" items="${year}" varStatus="status">
				<option value="${year.year }">${year.year }</option>
			</c:forEach>
		</select>
	</div>
</div>
<div class="section">
	<div class="grid-full" id="cloud"></div>
	<div class="h-group2">
		<div>
			<h3>상위 10위 키워드</h3>
		</div>
	</div>
	<div class="grid-full" id="bar"></div>

	<!-- 연도별 및 신청경로별 민원 현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>연도별 및 신청경로별 민원 현황</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart3' height='120px;'></canvas>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 10%;">
				<col style="width: 30%;">
				<col style="width: 30%;">
				<col style="width: 30%;">
			</colgroup>
			<thead>
				<tr>
					<th>신청경로</th>
					<th>2018년(건)</th>
					<th>2019년(건)</th>
					<th>2020년(건)</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />
			<c:set var="sum_two" value="0" />
			<c:set var="sum_three" value="0" />

			<tbody>
				<c:forEach items="${list3_1 }" var="list3_1" varStatus="status">
					<tr>
						<td>${list3_1.cate_nm }</td>
						<td><fmt:formatNumber value="${list3_1.cnt}" pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list3_2[status.index].cnt}"
								pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list3_3[status.index].cnt}"
								pattern="#,###" /></td>
					</tr>
					<c:set var="sum_one" value="${sum_one + list3_1.cnt}" />
					<c:set var="sum_two" value="${sum_two + list3_2[status.index].cnt}" />
					<c:set var="sum_three"
						value="${sum_three + list3_3[status.index].cnt}" />
				</c:forEach>
				<tr>
					<td>총합계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_two}" pattern="#,###" /></td>
					<td><fmt:formatNumber value="${sum_three}" pattern="#,###" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- 연도별 및 신청경로별 민원 현황 종료 -->



</div>
<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="https://d3js.org/d3.v4.js"></script>
<script src="../visual/common/js/Chart.min.js"></script>
<script
	src="https://cdn.jsdelivr.net/gh/holtzy/D3-graph-gallery@master/LIB/d3.layout.cloud.js"></script>
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=cc46f2d48285e645648b9d4a912d6346"></script>
<script>
chart1();
chart2();


function chart1(){
	var year = $('#year').val();
	var words2 = [];
	$("#cloud").html("<div id='my_dataviz'></div>");
	$.ajax({
		url:'/visual/proc-parking.do',
		type:'post',
		data:{year : year},
		success:function(data){
			var ctx = "";
			$.each(data.list, function(key, values){
				words2.push({word : `\${values.keyword}`, size : `\${values.total}`/3});
			});
				var myWords = words2

			// set the dimensions and margins of the graph
			var margin = {top: 10, right: 10, bottom: 10, left: 10},
			    width = 1000 - margin.left - margin.right,
			    height = 450 - margin.top - margin.bottom;

			// append the svg object to the body of the page
			var svg = d3.select("#my_dataviz").append("svg")
			    .attr("width", width + margin.left + margin.right)
			    .attr("height", height + margin.top + margin.bottom)
			  .append("g")
			    .attr("transform",
			          "translate(" + margin.left + "," + margin.top + ")");

			// Constructs a new cloud layout instance. It run an algorithm to find the position of words that suits your requirements
			// Wordcloud features that are different from one word to the other must be here
			var layout = d3.layout.cloud()
			  .size([width, height])
			  .words(myWords.map(function(d) { return {text: d.word, size:d.size}; }))
			  .padding(5)        //space between words
			  .rotate(function() { return ~~(Math.random() * 2) * 90; })
			  .fontSize(function(d) { return d.size; })      // font size of words
			  .on("end", draw);
			layout.start();

			// This function takes the output of 'layout' above and draw the words
			// Wordcloud features that are THE SAME from one word to the other can be here
			function draw(words) {
			  svg
			    .append("g")
			      .attr("transform", "translate(" + layout.size()[0] / 2 + "," + layout.size()[1] / 2 + ")")
			      .selectAll("text")
			        .data(words)
			      .enter().append("text")
			        .style("font-size", function(d) { return d.size; })
			        .style("fill", "#69b3a2")
			        .attr("text-anchor", "middle")
			        .style("font-family", "Impact")
			        .attr("transform", function(d) {
			          return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
			        })
			        .text(function(d) { return d.text; });
			}
		}, error: function(){
			alert("aaa");
		}
	})

}
function chart2(){
	var year = $('#year').val();

	var val1 = new Array();
	var label = new Array();
	$("#bar").html("<canvas id='Chart2' height='80px;'></canvas>");
	$.ajax({
		url:'/visual/proc-parking.do',
		type:'post',
		data:{year : year},
		success:function(data){
			var ctx = "";
			$.each(data.list2, function(key, values){
					val1.push(`\${values.total}`);
					label.push(`\${values.keyword}`);
			});
				var mydata = {
				        labels: label,
				        datasets: [{
				        	label: '키워드',
					        data: val1,
					        backgroundColor: "#3F45E8",
					    	hoverBackgroundColor: "blue"
					      }]
				    };
				
				var op = {
					    title: {
						      display: false,
						      text: '상위 10위 키워드'
						    },
						legend: {
							position: 'bottom'
						},
						tooltips: { mode: 'index',
							callbacks: {label: function(tooltipItem, data) {
								var label = data.datasets[tooltipItem.datasetIndex].label || '';
								  
					              if (label) {
					                  label += ': ';
					              }
						  		  let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
						  		  value = value.toString().replace(regexp, ',');
						  		  return label + value;
						  		}
						    }},
						scales: {
						      yAxes: [{
						    	  ticks: {
						    		  beginAtZero: true,
						    		  userCallback: function(value, index, values) {
						    			  value = value.toString().replace(regexp, ',');
						    		    return value;
						    		  },
						    		}
						      }],  
						      xAxes: [{
						      }],
						    }
						  };
			
				var ctx = document.getElementById("Chart2").getContext("2d");
				var myChart = new Chart(ctx, {
				    type: 'bar',
				    data: mydata,
				    options: op
				});
		}, error: function(){
			alert("aaa");
		}
	})

}

function chart3() {
	var cate_nm = new Array(); // 항목명칭(경로)
	var cnt_2018 = new Array(); // 민원수
	var cnt_2019 = new Array(); // 민원수
	var cnt_2020 = new Array(); // 민원수
	
	<c:forEach items="${list3_1}" var="list3_1" varStatus="status">
		cate_nm.push('${list3_1.cate_nm}');
		cnt_2018.push(${list3_1.cnt});
		cnt_2019.push(${list3_2[status.index].cnt});
		cnt_2020.push(${list3_3[status.index].cnt});
	</c:forEach>
    
	var mydata = {
        labels: cate_nm ,
        datasets: [{
            label: '2018년',
            data: cnt_2018,
            backgroundColor: "blue",
            hoverBackgroundColor: "blue"
        } , {
            label: '2019년',
            data: cnt_2019,
            backgroundColor: "Green",
            hoverBackgroundColor: "Green"
        }, {
            label: '2020년',
            data: cnt_2020,
            backgroundColor: "gray",
            hoverBackgroundColor: "gray"
        } ]
    };
    var op = { 		
        legend: {
            position: 'top',
          	onClick: null	
        },
        tooltips: {
        	enabled: false
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
            duration: 1,
            onComplete: function() {
            	var chartInstance = this.chart,
				ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

                this.data.datasets.forEach(function(dataset) {
                    for (var i = 0; i < dataset.data.length; i++) {
                        for (var key in dataset._meta) {
                            var model = dataset._meta[key].data[i]._model;
                            ctx.fillText(dataset.data[i].toLocaleString(), model.x, model.y - 13);
                        }
                    }
                });
            }
        },
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 35000, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart3 설정 최종
    var ctx = document.getElementById("Chart3").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}

chart3();

</script>