<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/include/include-header-visual.jspf"%>

<div class="contents-top">
	<div class="lnbmenu">
		<a href="/visual/disabled.do" class="btn-md on">불법 주차 단속</a>
	</div>
</div>
<div class="h-group2">
	<div>
		<h3>소화전 주변 불법 주차 단속 현황</h3>
	</div>
	<div>
		<span class="tit">연도</span> <select id="year" name="year"
			onchange="chart1();">
			<c:forEach var="year" items="${year}" varStatus="status">
				<option value="${year.year }">${year.year }</option>
			</c:forEach>
		</select>
	</div>
</div>
<div class="section">
	<div class="grid-full" id="mapwrap"></div>

	<!-- 행정동별 소화전 개수 시작 -->
	<div class="h-group2">
		<div>
			<h3>행정동별 소화전 개수</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart3' height='120px;'></canvas>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 40%;">
				<col style="width: 60%;">
			</colgroup>
			<thead>
				<tr>
					<th>행정동</th>
					<th>소화전 개수</th>
				</tr>
			</thead>

			<c:set var="sum_one" value="0" />

			<tbody>
				<c:forEach items="${list3 }" var="list3">
					<tr>
						<td>${list3.dong_nm }</td>
						<td><fmt:formatNumber value="${list3.cnt }" pattern="#,###" /></td>
					</tr>
					<c:set var="sum_one" value="${sum_one + list3.cnt}" />
				</c:forEach>
				<tr>
					<td>총합계</td>
					<td><fmt:formatNumber value="${sum_one}" pattern="#,###" /></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- 행정동별 소화전 개수 끝 -->

	<!-- 행정동별 불법주차 단속건수 숨김 
	<div class="h-group2">
		<div>
			<h3>행정동별 불법주차 단속건수</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart4' height='120px;'></canvas>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 30%;">
				<col style="width: 35%;">
				<col style="width: 35%;">
			</colgroup>
			<thead>
				<tr>
					<th>행정동</th>
					<th>대상 건물수</th>
					<th>평균 부과금액</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach items="${list4 }" var="list4">
					<tr>
						<td>${list4.dong_nm }</td>
						<td><fmt:formatNumber value="${list4.buil_cnt }"
								pattern="#,###" /></td>
						<td><fmt:formatNumber value="${list4.amount_avg }"
								pattern="#,###" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	 -->

	<!-- 연도별/월별 불법주차 단속 현황 시작 -->
	<div class="h-group2">
		<div>
			<h3>연도별/월별 불법주차 단속 현황</h3>
		</div>
	</div>
	<div class="grid-full">
		<canvas id='Chart5' height='120px;'></canvas>
	</div>

	<div class="ect1">
		<table class="table" style="text-align: center;">
			<colgroup>
				<col style="width: 30%;">
				<col style="width: 30%;">
				<col style="width: 40%;">
			</colgroup>
			<thead>
				<tr>
					<th>연도</th>
					<th>월</th>
					<th>단속건수</th>
				</tr>
			</thead>

			<tbody>
				<c:set var="sum_five" value="0" />

				<c:forEach items="${list5 }" var="list5">
					<tr>
						<td>${list5.std_yy }년</td>
						<td><fmt:formatNumber value="${list5.std_mm }"
								pattern="#,###" />월</td>
						<td><fmt:formatNumber value="${list5.cnt }" pattern="#,###" /></td>
					</tr>
					<c:set var="sum_five" value="${sum_five + list5.cnt}" />
				</c:forEach>
				<tr>
					<td>총합계</td>
					<td>-</td>
					<td><fmt:formatNumber value="${sum_five}" pattern="#,###" /></td>
				</tr>

			</tbody>
		</table>
	</div>
	<!-- 연도별/월별 불법주차 단속 현황 끝 -->



</div>

<!--
Chart2 내용 숨김 
<div class="grid-full">
	<canvas id='Chart2' height='120px;'></canvas>
</div> 
-->


<%@ include file="/WEB-INF/include/include-footer-visual.jspf"%>
<script src="../visual/common/js/Chart.min.js"></script>
<script type="text/javascript"
	src="//dapi.kakao.com/v2/maps/sdk.js?appkey=cc46f2d48285e645648b9d4a912d6346"></script>
<script>
Chart.defaults.global.legend.labels.usePointStyle = true;
chart1();



$(function(){
    $('.btn-nbd>button').on('click',function(){
        $(this).parent().find('button').removeClass('on')
        $(this).parent().find('button').addClass('off')
        $(this).removeClass('off')
        $(this).addClass('on')
    });
});
function chart1(){
$("#mapwrap").html('<div style="width:1050px;height:520px;"><div id="map" style="width:1050px;height:500px;"></div></div>');
var year = $('#year').val();
var regexp = /\B(?=(\d{3})+(?!\d))/g;
$.ajax({
	url:'/visual/proc-fireplug.do',
	type:'post',
	data:{year:year},
	success:function(data){

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
	center: new kakao.maps.LatLng(36.3556250, 127.3837457), // 지도의 중심좌표
    level: 5 // 지도의 확대 레벨
    };
var imageSrc = '../visual/images/red_pin.png', // 마커이미지의 주소입니다    
imageSize = new kakao.maps.Size(8, 8), // 마커이미지의 크기입니다
imageOption = {offset: new kakao.maps.Point(8, 8)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
var markerArray = new Array();
//var markerImage = new kakao.maps.MarkerImage('../../../images/visual/blue_pin.png', imageSize, imageOption) // 마커가 표시될 위치입니다
//마커의 이미지정보를 가지고 있는 마커이미지를 생성합니다

var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption),
markerPosition = new kakao.maps.LatLng(37.54699, 127.09598); // 마커가 표시될 위치입니다

var map = new kakao.maps.Map(mapContainer, mapOption),
    infowindow = new kakao.maps.InfoWindow({removable: true});
var cont = new Array();
var llng = new Array();
$.each(data.list, function(key, values){
	cont.push("<div style='font-size:10pt; padding:5px 10px 0px;text-align: center; vertical-align: middle; white-space:nowrap;'>"+ `\${values.addr}`+"</div>");
	llng.push(new kakao.maps.LatLng(`\${values.y}` , `\${values.x}`));
})

var positions = [];
	for(var i = 0; i < llng.length; i ++){
		positions.push(
	    {
	        content: cont[i], 
	        latlng: llng[i]
	    },
	    )
	}


// 마커 이미지의 이미지 주소입니다
for (var i = 0; i < positions.length; i ++) {
//마커 이미지의 이미지 크기 입니다

    var marker = new kakao.maps.Marker({
        map: map, // 마커를 표시할 지도
        position: positions[i].latlng, // 마커를 표시할 위치
        image: markerImage
    });
    
    var infowindow = new kakao.maps.InfoWindow({
        content: positions[i].content // 인포윈도우에 표시할 내용
    });
    
    kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
    kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
 
}

function makeOverListener(map, marker, infowindow) {
    return function() {
        infowindow.open(map, marker);
    };
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
function makeOutListener(infowindow) {
    return function() {
        infowindow.close();
    };
}

	}, error: function(){
		alert("aaa");
	}
})
}
</script>
<!-- 
<script>
function chart2() {
	var keyword = new Array(); // 키워드
	var one = new Array(); // one
	var two = new Array(); // one
	var three = new Array(); // one

    <c:forEach items="${list3}" var="list3" >
		keyword.push('${list3.keyword}');
		one.push('${list3.one}');
		two.push('${list3.two}');
		three.push('${list3.three}');
	</c:forEach>
    
	var mydata = {
        labels: keyword ,
        datasets: [{
            label: '2018',
            data: one,
            backgroundColor: "#3F45E8",
            hoverBackgroundColor: "blue",
            borderColor : "blue",
            fill:false
            
        } /* , {
            label: '2019',
            data: two,
            backgroundColor: "red",
            hoverBackgroundColor: "red",
            fill:false
        }, {
            label: '2020',
            data: three,
            backgroundColor: "gray",
            hoverBackgroundColor: "gray",
            fill:false
        } */ ]
    };
    var op = { 		
        legend: {
            /* position: 'top' */
           	display: false
        },
        tooltips: {
        	/* enabled: false */
            mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            }
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 3000, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart2 설정 최종
    var ctx = document.getElementById("Chart2").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: mydata,
        options: op
    });
}

chart2();
</script>
 -->

<script>
function chart3() { 
	var dong_nm = new Array(); // 항목명칭(경로)
	var cnt = new Array(); // 민원수

	<c:forEach items="${list3}" var="list3">
		dong_nm.push('${list3.dong_nm}');
		cnt.push(${list3.cnt});
	</c:forEach>
    
	var mydata = {
        labels: dong_nm ,
        datasets: [{
            label: '2018',
            data: cnt,
            backgroundColor: "navy",
            hoverBackgroundColor: "navy"
        } ]
    };
    var op = { 		
        legend: {
            display : false
        },
        tooltips: {
        	enabled: false
            /* mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            } */
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 100, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart3 설정 최종
    var ctx = document.getElementById("Chart3").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}

chart3();
</script>

<script>
function chart4() {
	var dong_nm = new Array(); // 항목명칭(경로)
	var buil_cnt = new Array(); // 민원수

	<c:forEach items="${list4}" var="list4">
		dong_nm.push('${list4.dong_nm}');
		buil_cnt.push(${list4.buil_cnt});
	</c:forEach>
    
	var mydata = {
        labels: dong_nm ,
        datasets: [{
            data: buil_cnt,
            backgroundColor: "navy",
            hoverBackgroundColor: "navy"
        } ]
    };
    var op = { 		
        legend: {
            display : false
        },
        tooltips: {
        	enabled: false
            /* mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            } */
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 400, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                 }
             }],
             xAxes: [{}],
        }
    };

	// chart4 설정 최종
    var ctx = document.getElementById("Chart4").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: mydata,
        options: op
    });
}

chart4();
</script>

<script>
function chart5() {
	var std_yy = new Array();
	var std_mm = new Array();
	var cnt = new Array();


	<c:forEach items="${list5}" var="list5">
		std_yy.push('${list5.std_yy}');
		std_mm.push(${list5.std_mm} + '월');
		cnt.push(${list5.cnt});

	</c:forEach>
	var mydata = {
        labels: std_mm ,
        datasets: [{
            label: '2018',
            data: cnt,
            lineTension: '0', // 꺽은선
            borderColor: "blue",
            backgroundColor: "#3F45E8",
            hoverBackgroundColor: "blue",
           	fill: false
        } ]
    };
    var op = { 		
        legend: {
            display : false
        },
        tooltips: {
        	enabled: false
            /* mode: 'index',
            callbacks: {
                label: function(tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    let value = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    value = value.toString().replace(regexp, ',');
                    return label + value;
                }
            } */
        },
        hover: {
			animationDuration: 0
		},
		animation: { // 차트 위에 숫자
			duration: 1,
			onComplete: function () {
				var chartInstance = this.chart,
					ctx = chartInstance.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = 'black';
				ctx.textAlign = 'right';
				ctx.textBaseline = 'bottom';

				this.data.datasets.forEach(function (dataset, i) {
					var meta = chartInstance.controller.getDatasetMeta(i);
					meta.data.forEach(function (bar, index) {
						var data = dataset.data[index];							
						ctx.fillText(data, bar._model.x, bar._model.y - 5);
					});
				});
			}
		},
        scales: {
            yAxes: [{
                 ticks: {
                	 suggestedMax: 300, // 최대값
                     beginAtZero: true,
                     fontSize : 14, // 폰트 사이즈
                     userCallback: function(value, index, values) {
                         value = value.toString().replace(regexp, ',');
                         return value;
                     },
                     
                 }
             }],
             xAxes: [{
	        	 display: true,
	             scaleLabel: {
	                 display: true,
	                 labelString: '2019년' + '                                                                                                                                   	                  ' + '2020년'
	                }
             }],
        }
    };

	// chart5 설정 최종
    var ctx = document.getElementById("Chart5").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: mydata,
        options: op
    });
}

chart5();
</script>
