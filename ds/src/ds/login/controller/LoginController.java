/*
 * 대전시 Login Class
 * @author: GDSC
 * 작업이력		작업일자		작업자		작업내용
 *				2020.07.09		서범원		최초작성
 * */
package ds.login.controller;

import java.io.File;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import ds.common.common.CommandMap;
import ds.common.service.CommonService;
import ds.common.util.CsvFileReader;
import ds.common.util.MailSender;
import ds.common.util.SecurityUtil;
import ds.common.util.TestApp;
import ds.login.service.LoginService;

@Controller
public class LoginController {
	Logger log = Logger.getLogger(this.getClass());

	@Resource(name = "loginService")
	private LoginService loginService;

	@Resource(name = "commonService")
	private CommonService commonService;

	@Resource(name = "uploadPath")
	String uploadPath;

	/**
	 * 로그인 화면 Open
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login.do")
	public ModelAndView openLogin(HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("/login/login");
		return mv;

	}

	/**
	 * Login 정보 확인 후 사용자 Login 정보 Return
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/login/proc-login-info.do")
	public void selectLoginInfo(CommandMap commandMap, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JSONObject jObj = new JSONObject();

		SecurityUtil securityUtil = new SecurityUtil();

		String pwd = (String) commandMap.get("user_passwd");

		String rtn = securityUtil.encryptSHA256(pwd);
		commandMap.put("user_passwd", rtn);

		Map<String, Object> map = loginService.selectUserInfo(commandMap.getMap());

		// id와 Password로 Select한 정보가 없을 경우
		if (map == null || map.isEmpty()) {
			HashMap failMap = new HashMap();
			failMap.put("result_code", "F");
			jObj.put("result", failMap);
		} else {

			String sUserId = map.get("user_id").toString();

			// Session 처리
			session.setAttribute("sessionKeyUserId", sUserId);
			session.setAttribute("page_athr_cd", map.get("page_athr_cd").toString());
			session.setAttribute("rw_athr_cd", map.get("rw_athr_cd").toString());

			map.put("result_code", "S");
			jObj.put("result", map);
		}

		response.addHeader("Content-Type", "text/plain");
		OutputStream out = response.getOutputStream();
		out.write(jObj.toString().getBytes("UTF-8"));
		out.flush();
		out.close();

	}

	/**
	 * 회원가입 화면 Open
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/join.do")
	public ModelAndView openJoin() throws Exception {
		ModelAndView mv = new ModelAndView("/login/join");

		// 공통코드 selectbox 처리
		// 국번
		List<Map<String, Object>> tonolList = commonService.selectCmnCdSelectBox("C00004");
		mv.addObject("tonolList", tonolList);

		return mv;

	}

	/**
	 * 회원가입: 이메일 중복 Check
	 * 
	 * @param commandMap
	 * @param session
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/proc-check-email.do")
	public void checkEmail(CommandMap commandMap, HttpServletResponse response) throws Exception {
		JSONObject jObj = new JSONObject();

		int iChkRtn = loginService.checkEmail(commandMap.getMap());

		jObj.put("result", iChkRtn);

		response.addHeader("Content-Type", "text/plain");
		OutputStream out = response.getOutputStream();
		out.write(jObj.toString().getBytes("UTF-8"));
		out.flush();
		out.close();

	}

	/**
	 * 회원가입: 회원가입 사용자 등록 메일 발송 --> 회원 정보 Insert
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/proc-regist-user.do")
	public ModelAndView procJoin(CommandMap commandMap, HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView("jsonView");

		String resultCode = "ok";
		String resultMsg = "";

		// 관리자 이메일 Select
		Map<String, Object> adminParmMap = new HashMap<>();
		adminParmMap.put("major_cd", "C00005");
		adminParmMap.put("minor_cd", "001");

		// 공통코드 단건 Select
		// 관리자 Email 주소 Select
		Map<String, Object> adminEmailMap = commonService.selectCmnCd(adminParmMap);
		String sSenderEmail = (String) adminEmailMap.get("minor_nm");

		try {

			try {

				// 메일 발송
				MailSender mailSender = new MailSender();

				Map<String, String> mailResult = mailSender.sendMail("admin", request, sSenderEmail, "dummy");

				if (mailResult.get("resultCode").equals("fail")) {
					resultCode = "fail";
					resultMsg = "메일발송 중 문제가 발생했습니다.";
					throw new Exception();
				}

				// 회원 정보 Insert
				try {

					SecurityUtil securityUtil = new SecurityUtil();

					String pwd = (String) commandMap.get("user_passwd");

					String rtn = securityUtil.encryptSHA256(pwd);
					commandMap.put("user_passwd", rtn);

					// 사용자 등록
					loginService.insertUser(commandMap.getMap());

				} catch (Exception e) {
					resultCode = "fail";
					resultMsg = "회원 등록 처리 중 문제가 발생했습니다.";
				}

			} catch (Exception e) {
				resultCode = "fail";
				resultMsg = "메일발송 중 문제가 발생했습니다.";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		mv.addObject("resultCode", resultCode);
		mv.addObject("resultMsg", resultMsg);

		return mv;

	}

	/**
	 * 관리자 로그인 시 업무 선택 화면 Open
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login-sadm.do")
	public ModelAndView openSadmLogin() throws Exception {
		ModelAndView mv = new ModelAndView("/login/login-sadm");
		return mv;

	}

	/**
	 * 비밀번호 찾기 화면 Open
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/find-email.do")
	public ModelAndView openFindEmail() throws Exception {
		ModelAndView mv = new ModelAndView("/login/find-email");

//    	공통코드 selectbox 처리
		// 국번
		List<Map<String, Object>> tonolList = commonService.selectCmnCdSelectBox("C00004");
		mv.addObject("tonolList", tonolList);

		return mv;

	}

	/**
	 * E-mail 찾기: Email 정보 Select
	 * 
	 * @param commandMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/proc-find-email.do")
	public ModelAndView selectFineEmail(CommandMap commandMap) throws Exception {

		ModelAndView mv = new ModelAndView("jsonView");

		// 목록 화면
		List<Map<String, Object>> list = loginService.selectFindEmail(commandMap.getMap());

		mv.addObject("list", list);
		if (list.size() > 0) {
			mv.addObject("total", list.get(0).get("total_count"));
		} else {
			mv.addObject("total", 0);
		}

		return mv;

	}

	/**
	 * 비밀번호 찾기 화면 Open
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/find-password.do")
	public ModelAndView openFindpassword() throws Exception {
		ModelAndView mv = new ModelAndView("/login/find-password");

		return mv;

	}

	/**
	 * 비밀번호 찾기 처리
	 * 
	 * @param commandMap
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/proc-find-password.do")
	public void findPassword(CommandMap commandMap, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		JSONObject jObj = new JSONObject();
		HashMap failMap = new HashMap();

		Map<String, Object> map = loginService.findPassword(commandMap.getMap());

		// id와 Password로 Select한 정보가 없을 경우
		if (map == null || map.isEmpty()) {
			failMap.put("result_code", "N");
			jObj.put("result", failMap);
		} else {

			// 공통코드 단건 Select
			// 관리자 Email 주소 Select
			// 관리자 이메일 Select
			Map<String, Object> adminParmMap = new HashMap<>();
			adminParmMap.put("major_cd", "C00005");
			adminParmMap.put("minor_cd", "001");

			Map<String, Object> adminEmailMap = commonService.selectCmnCd(adminParmMap);
			String sSenderEmail = (String) adminEmailMap.get("minor_nm");

			// "0000"로 Password 생성
			SecurityUtil securityUtil = new SecurityUtil();
			// String sRandomPasswd = StringUtil.randomStr(12) ;
			String sRandomPasswd = "0000";

			try {

				try {

					// 메일 발송
					MailSender mailSender = new MailSender();

					Map<String, String> mailResult = mailSender.sendMail("passwdChg", request, sSenderEmail,
							sRandomPasswd);

					if (mailResult.get("resultCode").equals("fail")) {
						map.put("result_code", "F");
						map.put("result_msg", "메일발송 중 문제가 발생했습니다.");
						jObj.put("result", map);
						throw new Exception();
					}

					/* 생성한 Password, 암호초기화여부 사용자 정보 update */
					try {

						/* 난수로 생성한 암호 암호화 처리 */
						String rtn = securityUtil.encryptSHA256(sRandomPasswd);

						/* 생성한 Password, 암호초기화여부 사용자 정보 update */
						commandMap.put("user_passwd", rtn);

						loginService.updateFindPassword(commandMap.getMap());

						map.put("result_code", "S");
						map.put("result_msg", "성공");
						jObj.put("result", map);

					} catch (Exception e) {
						map.put("result_code", "F");
						map.put("result_msg", "비밀번호 변경 처리 중 문제가 발생했습니다.");
						jObj.put("result", map);
					}

				} catch (Exception e) {
					map.put("result_code", "F");
					map.put("result_msg", "메일발송 중 문제가 발생했습니다.");
					jObj.put("result", map);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		response.addHeader("Content-Type", "text/plain");
		OutputStream out = response.getOutputStream();
		out.write(jObj.toString().getBytes("UTF-8"));
		out.flush();
		out.close();

	}

	/**
	 * 비밀번호 변경 화면 Open
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/change-password.do")
	public ModelAndView openChngPassword() throws Exception {
		ModelAndView mv = new ModelAndView("/login/change-password");
		return mv;

	}

	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/proc-change-password.do")
	public ModelAndView changePassword(CommandMap commandMap, HttpSession session) throws Exception {
		ModelAndView mv = new ModelAndView("jsonView");

		String resultCode = "ok";
		String resultMsg = "";

		String sUserId = (String) session.getAttribute("sessionKeyUserId");

		SecurityUtil securityUtil = new SecurityUtil();

		String pwd = (String) commandMap.get("user_passwd");

		String rtn = securityUtil.encryptSHA256(pwd);
		commandMap.put("user_passwd", rtn);
		commandMap.put("user_id", sUserId);

		/* 비밀번호 변경 */
		loginService.changePassword(commandMap.getMap());

		// Session 초기화
		session.invalidate();

		mv.addObject("resultCode", resultCode);
		mv.addObject("resultMsg", resultMsg);

		return mv;

	}

	/**
	 * Logout 처리
	 * 
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/login/proc-logout.do")
	public ModelAndView logOut(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mv = new ModelAndView("redirect:/login.do");

		// Session 초기화
		session.invalidate();
		return mv;
	}

	/*
	 * menual 다운로드
	 */

	@RequestMapping(value = "/login/menualDownload.do")
	public void downloadFile(CommandMap commandMap, HttpServletResponse response) throws Exception {

		// local위치
		byte fileByte[] = FileUtils.readFileToByteArray(new File("C:\\menual\\메뉴얼.docx"));
		// NAS위치
		// byte fileByte[] = FileUtils.readFileToByteArray(new
		// File("/usr/local/tomcat/webapps/pdf/PIGD_케어지수_"+storedFileName+".pdf"));

		response.setContentType("application/octet-stream");
		response.setContentLength(fileByte.length);
		response.setHeader("Content-Disposition",
				"attachment; fileName=\"" + URLEncoder.encode("메뉴얼.docx", "UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.getOutputStream().write(fileByte);

		response.getOutputStream().flush();
		response.getOutputStream().close();
	}

	@RequestMapping(value = "/table.do")
	public ModelAndView testTable(String file_cate, ModelAndView mav) throws Exception {

//		List<Map<String, Object>> fileList = loginService.selectFileList(file_cate);

		mav.setViewName("/login/table");
//		mav.addObject("file", fileList);

		return mav;
	}

	// 파일 등록 페이지
	@RequestMapping(value = "/fileInsert.do")
	public String fileInsert(@RequestParam String arg1, Model mav) throws Exception {

		// 파일 존재 여부 체크
		int count = loginService.fileCount(arg1);
	//	Map<String, Object> resultMap = loginService.selectFileInfo(map, arg1);
		
//		mav.setViewName("/login/fileInsert");
//		mav.addObject("arg1", arg1);
//		mav.addObject("test", count);

		mav.addAttribute("arg1", arg1);
		mav.addAttribute("test", count);
		
		return "login/fileInsert";
	}

	// 파일 등록
	@RequestMapping(value = "/fileUploadAction.do", method = RequestMethod.POST)
	public void fileUploadAction(MultipartHttpServletRequest mpRequest, String arg1)
			throws Exception {
		
		// 파일 존재 여부
		int count = loginService.fileCount(arg1);
		
		// 파일 등록
		if (count == 1) {			
			loginService.fileDelete(arg1);
			loginService.insertData(mpRequest, arg1);
		} else {
			loginService.insertData(mpRequest, arg1);
		}
		
		//return "redirect:/table.do";
	}
	
	// 작업중 csv 파일 업로드
	@RequestMapping(value = "/csvFileInsert.do")
	public String csvFileUploadAction(@RequestParam String FILE_cate, Model m) throws Exception {
		System.out.println("csvFileInsert.do : " + FILE_cate);
		
		int count = loginService.fileCount(FILE_cate);
		m.addAttribute("FILE_cate", FILE_cate);
		
		// 파일명 같이 넘겨야 함. 체크
		
//		if (count == 1) {
//			loginService.insertCsvFile(FILE_cate);
//			
//		} 
		
		//mav.setViewName("redirect:/table.do");
		//mav.addObject("arg1", arg1);
		return "login/test";
	}

	
	// 파일 다운로드
	@RequestMapping(value = "/fileDown.do")
	public String fileDown(@RequestParam Map<String, Object> map, HttpServletResponse response,
			@RequestParam String FILE_cate) throws Exception {
		
		System.out.println("map!!!!!! : " + map);
		System.out.println("response!!!!!!!! : " + response);
		System.out.println("FILE_cate!!!!!! : " + FILE_cate);
		
		int count = loginService.fileCount(FILE_cate);
		
		if (count == 1) {
			Map<String, Object> resultMap = loginService.selectFileInfo(map, FILE_cate);
			
			String storedFileName = (String) resultMap.get("stored_file_name");
			String originalFileName = (String) resultMap.get("org_file_name");
			String FILE_CATE = FILE_cate;
			
			byte fileByte[] = org.apache.commons.io.FileUtils.readFileToByteArray(new File(uploadPath + storedFileName));

			response.setContentType("application/octet-stream");
			response.setContentLength(fileByte.length);
			response.setHeader("Content-Disposition",
					"attachment; fileName=\"" + URLEncoder.encode(originalFileName, "UTF-8") + "\";");
			response.getOutputStream().write(fileByte);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} 
		
		return "redirect:/table.do";
		
	}
	
	

}
