package ds.login.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import ds.common.dao.AbstractDAO;

@Repository("loginDAO")
public class LoginDAO extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectUserInfo(Map<String, Object> map) {
		return (Map<String, Object>) selectOne("login.selectUserInfo", map);
	}

	public int checkEmail(Map<String, Object> map) {
		return (int) selectOne("login.checkEmail", map);
	}

	public void insertUser(Map<String, Object> map) {
		insert("login.insertUser", map);

	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectFindEmail(Map<String, Object> map) {
		return (List<Map<String, Object>>) selectList("login.selectFindEmail", map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> findPassword(Map<String, Object> map) {
		return (Map<String, Object>) selectOne("login.findPassword", map);
	}

	public void updateFindPassword(Map<String, Object> map) {
		update("login.updateFindPassword", map);
	}

	public void changePassword(Map<String, Object> map) {
		update("login.changePassword", map);
	}

	// 파일첨부
	public void insertData(Map<String, Object> map) {
		insert("login.insertData", map);
	}

	/*
	 * // 첨부파일 조회
	 * 
	 * @SuppressWarnings("unchecked") public List<Map<String, Object>>
	 * selectFileList(String file_cate) throws Exception { return
	 * selectList("login.selectFileList", file_cate); }
	 */

	// 파일 존재 여부
	public int fileCount(String file_cate) throws Exception {
		return (int) selectOne("login.fileCount", file_cate);
	}

	public void fileDelete(String file_cate) throws Exception {
		delete("login.fileDelete", file_cate);
	}

	// 첨부파일 다운
	@SuppressWarnings("unchecked")
	public Map<String, Object> selectFileInfo(Map<String, Object> map) throws Exception {
		return (Map<String, Object>) selectOne("login.selectFileInfo", map);
	}

	// csv 파일 업르도
	public void insertCsvFile(Map<String, Object> list) {
		insert("login.insertCsvFile", list);
	}
}
