package ds.login.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import ds.common.util.CsvFileReader;
import ds.common.util.FileUtils;
import ds.login.dao.LoginDAO;
import oracle.sql.CharacterSet;

@Service("loginService")
public class LoginServiceImpl implements LoginService {

	Logger log = Logger.getLogger(this.getClass());

	@Resource(name = "loginDAO")
	private LoginDAO loginDAO;

	@Resource(name = "fileUtils")
	private FileUtils fileUtils;
	
	

	/**
	 * Login 정보 확인 후 사용자 Login 정보 Return
	 */
	@Override
	public Map<String, Object> selectUserInfo(Map<String, Object> map) throws Exception {
		Map<String, Object> resultMap = loginDAO.selectUserInfo(map);
		return resultMap;
	}

	/**
	 * 회원가입: Email 중복 Check
	 */
	@Override
	public int checkEmail(Map<String, Object> map) throws Exception {

		int iChkRtn = loginDAO.checkEmail(map);

		return iChkRtn;
	}

	/**
	 * 회원가입: 사용자 등록
	 */
	@Override
	public void insertUser(Map<String, Object> map) throws Exception {

		loginDAO.insertUser(map);
	}

	/**
	 * E-mail 찾기: 가입자 Email 정보 Select
	 */
	@Override
	public List<Map<String, Object>> selectFindEmail(Map<String, Object> map) throws Exception {
		return loginDAO.selectFindEmail(map);
	}

	/**
	 * 비밀번호 찾기
	 */
	@Override
	public Map<String, Object> findPassword(Map<String, Object> map) throws Exception {
		Map<String, Object> resultMap = loginDAO.findPassword(map);
		return resultMap;
	}

	/**
	 * 비밀번호 찾기: 난수 생성 Password, 암호초기화여부 Update
	 */
	@Override
	public void updateFindPassword(Map<String, Object> map) throws Exception {
		loginDAO.updateFindPassword(map);

	}

	/**
	 * 비밀번호 변경
	 */
	@Override
	public void changePassword(Map<String, Object> map) throws Exception {
		loginDAO.changePassword(map);

	}

	@Override
	public void insertData(MultipartHttpServletRequest mpRequest, String arg1)
			throws Exception {

		Map<String, Object> list = fileUtils.parseInsertFileInfo(mpRequest, arg1);
		
//		int size = list.size();
//		for (int i = 0; i < size; i++) {
			loginDAO.insertData(list);
//		}

	}
	
	@Override
	public void insertCsvFile(String arg1) throws Exception {

		Map<String, Object> list = new HashedMap<String, Object>();
		
		BufferedReader lineReader = new BufferedReader(new InputStreamReader(new FileInputStream("D:\\upload\\sampleCsvFile.csv"), "UTF-8"));
		String lineText = "";
		
		lineReader.readLine();

		while ((lineText = lineReader.readLine()) != null) {
			String[] data = lineText.split(",");
			String sampleA = data[0];
			String sampleB = data[1];
			String sampleC = data[2];
			String sampleD = data[3];
			list.put("sampleA", sampleA);
			list.put("sampleB", sampleB);
			list.put("sampleC", sampleC);
			list.put("sampleD", sampleD);
			list.put("arg1", arg1);

			loginDAO.insertCsvFile(list);
		}
		
		lineReader.close();

	
	}

	/*
	 * @Override public List<Map<String, Object>> selectFileList(String file_cate)
	 * throws Exception { return loginDAO.selectFileList(file_cate); }
	 */

	// 파일 존재 여부
	@Override
	public int fileCount(String arg1) throws Exception {
		return loginDAO.fileCount(arg1);
	}

	@Override
	public void fileDelete(String arg1) throws Exception {
		loginDAO.fileDelete(arg1);
	}

	// 첨부파일 다운로드
	@Override
	public Map<String, Object> selectFileInfo(Map<String, Object> map, String FILE_cate) throws Exception {
		map.put("file_cate", FILE_cate);
		return loginDAO.selectFileInfo(map);
	}
	


}
