package ds.login.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface LoginService {

	Map<String, Object> selectUserInfo(Map<String, Object> map) throws Exception;

	int checkEmail(Map<String, Object> map) throws Exception;

	void insertUser(Map<String, Object> map) throws Exception;

	List<Map<String, Object>> selectFindEmail(Map<String, Object> map) throws Exception;

	Map<String, Object> findPassword(Map<String, Object> map) throws Exception;

	void updateFindPassword(Map<String, Object> map) throws Exception;

	void changePassword(Map<String, Object> map) throws Exception;

	// 파일 업로드
	public void insertData(MultipartHttpServletRequest mpRequest, String arg1) throws Exception;

	// 파일 조회
	// public List<Map<String, Object>> selectFileList(String file_cate) throws
	// Exception;

	// 파일 존재 여부
	public int fileCount(String arg1) throws Exception;

	// 파일 삭제
	public void fileDelete(String arg1) throws Exception;

	// 첨부파일 다운
	public Map<String, Object> selectFileInfo(Map<String, Object> map, String FILE_cate) throws Exception;

	// csv파일 업로드
	public void insertCsvFile(String arg1) throws Exception;
}
