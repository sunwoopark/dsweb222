package ds.common.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ds.common.service.CommonService;

public class MailSender {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="commonService")
    private CommonService commonService;
	
	/**
	 * 메일 발송
	 * String sendType : 메일을 받을 대상. customer / manager
	 * NoticeVO customerInfoVO : 발송될 내용
	 *   NoticeVO customerInfoVO, MailInfoVO mailInfoVO,
	 * @throws Exception 
	 */
	public Map<String,String> sendMail(String sendType, HttpServletRequest request, String pSenderEmail, String pPasswd) throws Exception {
		Map<String,String> resultMap = new HashMap<>();
		String resultCode = "ok";
		
		final String sendName = pSenderEmail ;
		//final String sendName = "chanbob79@korea.kr" ;
		
		//받는 계정, 메일 제목, 메일 내용
		String receivedName="", receivedEmail = "", receivedSubject = "", receivedMsg = "";
		
		/**
		 * passwdChg: 회원에게 발송하는 비밀번호 초기화 알림 메일
		 * member: 회원에게 발송하는 회원가입 승인 알림 메일
		 * admin: 관리자에게 발송하는 회원가입 승인 요청 있음 알림 메일
		 */
		switch (sendType) {
			case "passwdChg" :
				receivedSubject = "[대전시 관리자]비밀번호 초기화 알림 메일입니다.";
				break ;
			case "member" :
				receivedSubject = "[대전시 관리자]회원가입 승인 알림 메일입니다.";
				break ;
			case "admin" :
				receivedSubject = "[대전시 관리자]회원 가입 승인을 요청한 계정이 있습니다.";
				break ;
			default:
				resultMap.put("resultCode", "정의되지 않은 메일형태입니다.");
				return resultMap;
		}
		
		receivedName =  request.getParameter("user_nm") ;
		//receivedEmail = request.getParameter("user_id") + "@korea.kr" ;
		//receivedEmail = request.getParameter("user_id") + "@korea.kr" ;
		
		// test: 운영 시 삭제
		//receivedEmail = "chanbob79@korea.kr" ;
		System.out.println("request ID : " + request.getParameter("user_id"));
		receivedEmail = "czower011@gdsconsulting.co.kr" ;
		try {
			Properties props = System.getProperties();
			
			//SMTP 설정
			// Server IP 변경
			//props.put("mail.smtp.host", "121.130.91.138");	// HyperV Mail Server
			props.put("mail.smtp.host", "192.168.137.203");	//  Server HyperV Mail Server
			props.put("mail.smtp.port", "2525");

			Session session = Session.getDefaultInstance(props);
			// 운영 시 false
//			session.setDebug(false);
			session.setDebug(true);

			MimeMessage mimeMessage = new MimeMessage(session);
			
			mimeMessage.setFrom(new InternetAddress(sendName, "[대전시관리자]", "utf-8"));
			mimeMessage.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(receivedEmail));
			mimeMessage.setSubject(receivedSubject, "utf-8");

			Multipart multipart = new MimeMultipart();
			
			MimeBodyPart mimebodyTxt = new MimeBodyPart();

			// html 처리
			mimebodyTxt.setContent(setMsgHtml(receivedMsg, sendType, receivedName, request, pPasswd), "text/html; charset=utf-8");
			
			multipart.addBodyPart(mimebodyTxt);
			
			// 첨부 이미지 처리
			mimebodyTxt = new MimeBodyPart();
			
			// ServletContext 를 얻어온다.
			ServletContext context = request.getServletContext();
			File contextDir = null ;
			
			switch (sendType) {
			case "passwdChg" :
				contextDir = new File(context.getRealPath("/images/mail/mail-top-pass.jpg"));
				break ;
			case "member" :
				contextDir = new File(context.getRealPath("/images/mail/mail-top.jpg"));
				break ;
			case "admin" :
				contextDir = new File(context.getRealPath("/images/mail/mail-top-mbr.jpg"));
				break ;
			default:
				break ;
			}
			
			DataSource fds = new FileDataSource(contextDir);
	        mimebodyTxt.setDataHandler(new DataHandler(fds));
	        mimebodyTxt.setHeader("Content-ID","<image>");
	        
	        multipart.addBodyPart(mimebodyTxt);
			mimeMessage.setContent(multipart);
			
			Transport.send(mimeMessage);
		} catch(AddressException ae) {
//			System.out.println(" ********************* sendMail AddressException : " + ae.getMessage());
			resultCode = "fail";
		} catch(MessagingException me) {
//			System.out.println(" ********************* sendMail MessagingException : " + me.getMessage());
			resultCode = "fail";
		} catch(Exception e) {
//			System.out.println(" ********************* sendMail Exception : " + e.getMessage());
			resultCode = "fail";
		}

		resultMap.put("resultCode", resultCode);
		
		return resultMap;
	}

	/**
	 * 리턴될 HTML
	 */
	private String setMsgHtml(String msg, String sendType, String pReceivedName, HttpServletRequest request, String pPasswd){
		
		String sUrl = "" ;
		String sAltText = "" ;
		String sMsgText1 = "" ;
		String sMsgText2 = "" ;
		String sMsgText3 = "" ;
		String result = "";
		
		msg = msg.replaceAll("\r\n", "<br/>").replaceAll("\n", "<br/>");
		
		switch(sendType) {
		case "passwdChg" :
			
			sAltText = "비밀번호 변경" ;
			sUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/login.do";
			sMsgText1 = pPasswd ;
			sMsgText2 = "아래 URL 클릭을 통해서 비밀번호를 변경하시기 바랍니다." ;
			sMsgText3 = "로그인 바로가기" ;
			break ;
		case "member" :
			
			sAltText = "대전시 관리자 시스템" ;
			sUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/login.do";
			sMsgText1 = "회원가입 승인 알림 메일" ;
			sMsgText2 = "요청하신 회원가입이 정상적으로 승인되었습니다." ;
			sMsgText3 = sUrl ;
			break ;
		case "admin" :
			sAltText = "회원가입승인요청" ;
			sUrl = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/mng/login.do";
			sMsgText1 = "회원가입 승인 알림 메일" ;
			sMsgText2 = "회원 가입 승인을 요청한 계정이 있습니다." ;
			sMsgText3 = "ADMIN 바로가기" ;
			break ;
		default:
				result += "정의되지 않은 메일형태입니다." ;	
		}
		
		result += "<!DOCTYPE html>";
		result += "<html lang='ko'>";
		result += "	<head>";
		result += "		<meta charset='UTF-8'>";
		result += "		<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
		result += "	</head>";
		result += "	<body style='background:#ddd'>";
		result += " 	<div style='width:600px;margin:00 auto;background:#fff;padding-bottom:50px'>" ;
		result += "			<img src='cid:image' alt='#altText'>";
		result += "			<p style='font-size:20px;font-weight:bold;text-align:center;margin-bottom:0'>#msgText1</p>" ;
		result += "			<p style='font-size:16px;text-align:center;margin-top:10px'>#msgText2</p>";
		result += "			<p style='color:#006699;text-align:center;text-decoration: underline;'><a href='#sUrl' target='_blank'>#msgText3</a></p>";
		result += "		</div>";
		result += "	</body>";
		result += "</html>";

		result = result.replace("#altText", sAltText) ;
		result = result.replace("#sUrl", sUrl) ;
		result = result.replace("#msgText1", sMsgText1) ;
		result = result.replace("#msgText2", sMsgText2) ;
		result = result.replace("#msgText3", sMsgText3) ;
		
//		System.out.println("sUrl :"+sUrl);
//		System.out.println("result : " +result);
		
		return result;
	}
}