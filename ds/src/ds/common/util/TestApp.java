package ds.common.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

public class TestApp {

//	@Resource(name = "uploadPath")
	String filePath = "D:\\upload\\sampleCsvFile.csv";

	public List<Map<String, Object>> insertCVSFILE() throws Exception {

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> listMap = null;

		BufferedReader lineReader = new BufferedReader(new FileReader(filePath));
		String lineText = null;

		lineReader.readLine(); // skip header line

		while ((lineText = lineReader.readLine()) != null) {
			String[] data = lineText.split(",");
			listMap.put("sampleA", data[0]);
			listMap.put("sampleB", data[1]);
			listMap.put("sampleC", data[2]);
			listMap.put("sampleD", data[3]);
			list.add(listMap);
		}

		lineReader.close();

		return list;
	}

}
