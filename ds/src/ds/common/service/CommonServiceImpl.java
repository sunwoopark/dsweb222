package ds.common.service;

import java.util.List;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import ds.common.dao.CommonDAO;
 
@Service("commonService")
public class CommonServiceImpl implements CommonService{
    Logger log = Logger.getLogger(this.getClass());
     
    @Resource(name="commonDAO")
    private CommonDAO commonDAO;
    
    /**
     * 공통코드 Select
     */
	@Override
	public List<Map<String, Object>> selectCmnCdSelectBox(String majorCd) throws Exception {
		return commonDAO.selectCmnCdList(majorCd) ;
	}

	/**
	 * Use Log Insert
	 */
	@Override
	public Object insertUseLog(Map<String, Object> map) throws Exception {
		return commonDAO.insertUseLog(map);
	}

	/**
	 * Menu 권한 Select
	 */
	@Override
	public Map<String, Object> selectMenuArth(Map<String, Object> map) throws Exception {
		Map<String, Object> resultMap = commonDAO.selectMenuArth(map);
		return resultMap;
	}

	/**
	 * 관리자 이메일 정보 Select
	 */
	@Override
	public Map<String, Object> selectCmnCd(Map<String, Object> map) throws Exception {
		return commonDAO.selectCmnCd(map) ;
	}

}